package utils.matchers;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.util.*;

import static java.util.stream.Collectors.toList;

public class OptionalMatchers {

    @SafeVarargs
    public static <E> AllOfOrNotPresent<E> allOfOrNotPresent(E... allowedValues){
        return new AllOfOrNotPresent<>(allowedValues);
    }

    private static class AllOfOrNotPresent<T> extends TypeSafeMatcher<Collection<Optional<? extends T>>> {
        List<T> allowedValues = new ArrayList<>();

        @SafeVarargs
        protected AllOfOrNotPresent(T... allowedValues){
            this.allowedValues.addAll(Arrays.asList(allowedValues));
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("Optionals should be empty or produce in any order all these values " + this.allowedValues);
        }

        @Override
        protected boolean matchesSafely(Collection<Optional<? extends T>> optionals) {
            List<T> copyAllowedValues = new ArrayList<>(this.allowedValues);

            optionals.stream().filter(Optional::isPresent).forEach( opt -> copyAllowedValues.remove(opt.get()));
            return copyAllowedValues.isEmpty();
        }

        @Override
        protected void describeMismatchSafely(Collection<Optional<? extends T>> item, Description mismatchDescription) {
            mismatchDescription.appendText(" but produced " + item.stream().filter(Optional::isPresent).map(Optional::get).collect(toList()));
            long emptyCount = item.stream().filter(Optional::isEmpty).count();
            if(emptyCount > 0)
                mismatchDescription.appendText(" and " + emptyCount + " empty Optionals");
        }
    }
}
