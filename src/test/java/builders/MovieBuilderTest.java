package builders;

import domain.Movie;
import domain.Poster;
import domain.Subtitle;
import org.junit.Test;
import scanning.AbstractScanningTest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static utils.matchers.OptionalMatchers.allOfOrNotPresent;

public class MovieBuilderTest extends AbstractScanningTest {

    @Test
    public void testMovieBuilder() throws URISyntaxException, IOException {
        MovieBuilder builder = MovieBuilder.getNewBuilder();
        File movie1File = getFileFromTestResourceFolder("Root/Library/Movie1", "film1 (2019).1080p.DTS-5.1.mkv");
        File subtitle1File = getFileFromTestResourceFolder("Root/Library/Movie1", "film1 (2019).1080p.DTS-5.1.english.srt");
        File posterFile = getFileFromTestResourceFolder("Root/Library/Movie1", "Film 1 poster.jpg");
        Movie movie1 = builder
                .addMovieFile(movie1File)
                .addSubtitleFile(subtitle1File)
                .addPosterFile(posterFile)
                .build();

        assertThat(movie1.getFileName(), is("film1 (2019).1080p.DTS-5.1.mkv"));
        assertThat(movie1.getFileSize(), is(Files.size(movie1File.toPath())));
        assertThat(movie1.getTitle(), is("film1"));
        assertThat(movie1.getYear(), is(2019));
        assertThat(movie1.getPosters(), contains(new Poster("Film 1 poster.jpg", Files.size(posterFile.toPath()))));
        assertThat(movie1.getSubtitles(), contains(new Subtitle("film1 (2019).1080p.DTS-5.1.english.srt", Files.size(subtitle1File.toPath()), "english")));
        assertThat(movie1.getSubtitles().stream().map(Subtitle::getLanguage).collect(toList()), allOfOrNotPresent( "english"));
    }

    @Test
    public void testMultipleSubtitles() throws URISyntaxException, IOException {
        MovieBuilder builder = MovieBuilder.getNewBuilder();
        File movie2File = getFileFromTestResourceFolder("Root/Library/grouping/Movie2", "film2 (2019).m4v");
        File subtitle1File = getFileFromTestResourceFolder("Root/Library/grouping/Movie2", "film2 (2019).english.srt");
        File subtitle2File = getFileFromTestResourceFolder("Root/Library/grouping/Movie2", "film2 (2019).dutch.srt");
        File subtitle3File = getFileFromTestResourceFolder("Root/Library/grouping/Movie2", "film2 (2019).srt");
        File posterFile = getFileFromTestResourceFolder("Root/Library/grouping/Movie2", "poster_film2.png");
        Movie movie2 = builder
                .addMovieFile(movie2File)
                .addSubtitleFile(subtitle1File)
                .addSubtitleFile(subtitle2File)
                .addPosterFile(posterFile)
                .addSubtitleFile(subtitle3File)
                .build();

        assertThat(movie2.getSubtitles(), containsInAnyOrder(
                new Subtitle("film2 (2019).english.srt", Files.size(subtitle1File.toPath()), "english")
                ,new Subtitle("film2 (2019).dutch.srt", Files.size(subtitle2File.toPath()), "dutch")
                ,new Subtitle("film2 (2019).srt", Files.size(subtitle3File.toPath()), null))
        );

        assertThat(movie2.getSubtitles().stream().map(Subtitle::getLanguage).collect(toList()), allOfOrNotPresent("dutch", "english"));
    }

    @Test
    public void testMultiPartMovie() throws URISyntaxException, IOException {
        MovieBuilder builder = MovieBuilder.getNewBuilder();
        File movieCD1 = getFileFromTestResourceFolder("Root/Library/2PartMovie", "Film (2019).CD1.AC3-5.1.mkv");
        File movieCD2 = getFileFromTestResourceFolder("Root/Library/2PartMovie", "Film (2019).CD2.AC3-5.1.mkv");
        Movie movie = builder
                .addMovieFile(movieCD1)
                .addMovieFile(movieCD2)
                .build();

        assertThat(movie.getFileName(), is("Film (2019).AC3-5.1.mkv"));
        assertThat(movie.getFileSize(), is(Files.size(movieCD1.toPath())+Files.size(movieCD2.toPath())));
        assertThat(movie.getTitle(), is("Film"));
        assertThat(movie.getYear(), is(2019));

        builder = MovieBuilder.getNewBuilder();
        File moviePart1 = getFileFromTestResourceFolder("Root/Library/2PartMovie2", "Film (2018).Part 1.AC3-5.1.mkv");
        File moviePart2 = getFileFromTestResourceFolder("Root/Library/2PartMovie2", "Film (2018).Part 2.AC3-5.1.mkv");
        movie = builder
                .addMovieFile(moviePart1)
                .addMovieFile(moviePart2)
                .build();

        assertThat(movie.getFileName(), is("Film (2018).AC3-5.1.mkv"));
        assertThat(movie.getFileSize(), is(Files.size(moviePart1.toPath())+Files.size(moviePart2.toPath())));
        assertThat(movie.getTitle(), is("Film"));
        assertThat(movie.getYear(), is(2018));
    }

    @Test
    public void oldYearFormat() throws URISyntaxException, IOException {
        MovieBuilder builder = MovieBuilder.getNewBuilder();
        File movie3File = getFileFromTestResourceFolder("Root/Library/grouping/Movie3", "film3.2019.avi");
        File subtitle3File = getFileFromTestResourceFolder("Root/Library/grouping/Movie3", "film3.2019.english.srt");
        File posterFile = getFileFromTestResourceFolder("Root/Library/grouping/Movie3", "poster3.jpeg");

        Movie movie = builder
                .addMovieFile(movie3File)
                .addSubtitleFile(subtitle3File)
                .addPosterFile(posterFile)
                .build();

        assertThat(movie.getFileName(), is("film3.2019.avi"));
        assertThat(movie.getFileSize(), is(Files.size(movie3File.toPath())));
        assertThat(movie.getTitle(), is("film3"));
        assertThat(movie.getYear(), is(2019));
    }
}