package builders;

import domain.Poster;
import domain.Subtitle;
import domain.TVEpisode;
import domain.TVSeries;
import org.junit.Test;
import scanning.AbstractScanningTest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static utils.matchers.OptionalMatchers.allOfOrNotPresent;

public class TVSeriesBuilderTest extends AbstractScanningTest {
    @Test
    public void testTVSeriesBuilder() throws URISyntaxException, IOException {
        TVSeriesBuilder builder = TVSeriesBuilder.getNewBuilder();

        File episode1File = getFileFromTestResourceFolder("Root/Library/tvseries2/season 2", "episode.S02E01.mkv");
        File episode2File = getFileFromTestResourceFolder("Root/Library/tvseries2/season 2", "episode.S02E02.mkv");
        File episode1_dutch = getFileFromTestResourceFolder("Root/Library/tvseries2/season 2", "episode.S02E01.dutch.srt");
        File episode2_dutch = getFileFromTestResourceFolder("Root/Library/tvseries2/season 2", "episode.S02E02.dutch.srt");
        File episode1_unknown = getFileFromTestResourceFolder("Root/Library/tvseries2/season 2", "episode.S02E01.srt");
        File episode2_unknown = getFileFromTestResourceFolder("Root/Library/tvseries2/season 2", "episode.S02E02.srt");
        File poster_1 = getFileFromTestResourceFolder("Root/Library/tvseries2", "season 1.jpeg");
        File poster_2 = getFileFromTestResourceFolder("Root/Library/tvseries2", "season 2.png");

        TVSeries series = builder
                .setName("tvseries2")
                .addPosterFile(poster_1)
                .addPosterFile(poster_2)
                .addSubtitleFile(episode1_dutch)
                .addEpisodeFile(episode1File)
                .addSubtitleFile(episode1_unknown)
                .addSubtitleFile(episode2_dutch)
                .addEpisodeFile(episode2File)
                .addSubtitleFile(episode2_unknown)
                .build();

        assertThat(series.getName(), is("tvseries2"));
        assertThat(series.getPosters(), containsInAnyOrder(new Poster("season 1.jpeg", Files.size(poster_1.toPath())), new Poster("season 2.png", Files.size(poster_2.toPath()))));
        assertThat(series.getEpisodes(), containsInAnyOrder(new TVEpisode("episode.S02E01.mkv", Files.size(episode1File.toPath())), new TVEpisode("episode.S02E02.mkv", Files.size(episode2File.toPath()))));

        TVEpisode episode1 = series.getEpisodes().stream().filter(e -> e.getFileName().equals("episode.S02E01.mkv")).findFirst().get();
        assertThat(episode1.getEpisodeNumbers(), is(List.of(1)));
        assertThat(episode1.getSubtitles(),
                containsInAnyOrder(new Subtitle("episode.S02E01.dutch.srt", Files.size(episode1_dutch.toPath()), "dutch"), new Subtitle("episode.S02E01.srt", Files.size(episode1_unknown.toPath()), null)));

        TVEpisode episode2 = series.getEpisodes().stream().filter(e -> e.getFileName().equals("episode.S02E02.mkv")).findFirst().get();
        assertThat(episode2.getEpisodeNumbers(), is(List.of(2)));
        assertThat(episode2.getSubtitles(),
                containsInAnyOrder(new Subtitle("episode.S02E02.dutch.srt", Files.size(episode2_dutch.toPath()), "dutch"), new Subtitle("episode.S02E02.srt", Files.size(episode2_unknown.toPath()), null)));

        assertThat(episode1.getSubtitles().stream().map(Subtitle::getLanguage).collect(toList()), allOfOrNotPresent("dutch"));
        assertThat(episode2.getSubtitles().stream().map(Subtitle::getLanguage).collect(toList()), allOfOrNotPresent("dutch"));
    }

    @Test
    public void testNoSeason() throws URISyntaxException, IOException {
        TVSeriesBuilder builder = TVSeriesBuilder.getNewBuilder();

        File episode1File = getFileFromTestResourceFolder("Root/Library/tvseries", "episode.E01.mkv");
        File episode2File = getFileFromTestResourceFolder("Root/Library/tvseries", "episode.E02.avi");
        File poster = getFileFromTestResourceFolder("Root/Library/tvseries", "tv_poster.jpg");

        TVSeries series = builder
                .setName("tvseries")
                .addPosterFile(poster)
                .addEpisodeFile(episode1File)
                .addEpisodeFile(episode2File)
                .build();

        assertThat(series.getName(), is("tvseries"));
        assertThat(series.getPosters(), containsInAnyOrder(new Poster("tv_poster.jpg", Files.size(poster.toPath()))));

        TVEpisode episode1 = series.getEpisodes().stream().filter(e -> e.getFileName().equals("episode.E01.mkv")).findFirst().get();
        assertThat(episode1.getEpisodeNumbers(), is(List.of(1)));
        TVEpisode episode2 = series.getEpisodes().stream().filter(e -> e.getFileName().equals("episode.E02.avi")).findFirst().get();
        assertThat(episode2.getEpisodeNumbers(), is(List.of(2)));
    }

    @Test
    public void testOnlyUnknownLanguage() throws URISyntaxException, IOException {
        TVSeriesBuilder builder = TVSeriesBuilder.getNewBuilder();

        File episode1File = getFileFromTestResourceFolder("Root/Library/tvseries2/season 2", "episode.S02E01.mkv");
        File episode2File = getFileFromTestResourceFolder("Root/Library/tvseries2/season 2", "episode.S02E02.mkv");
        File episode1_unknown = getFileFromTestResourceFolder("Root/Library/tvseries2/season 2", "episode.S02E01.srt");
        File episode2_unknown = getFileFromTestResourceFolder("Root/Library/tvseries2/season 2", "episode.S02E02.srt");

        TVSeries series = builder
                .setName("tvseries2")
                .addEpisodeFile(episode1File)
                .addSubtitleFile(episode1_unknown)
                .addEpisodeFile(episode2File)
                .addSubtitleFile(episode2_unknown)
                .build();

        assertThat(series.getName(), is("tvseries2"));

        TVEpisode episode1 = series.getEpisodes().stream().filter(e -> e.getFileName().equals("episode.S02E01.mkv")).findFirst().get();
        TVEpisode episode2 = series.getEpisodes().stream().filter(e -> e.getFileName().equals("episode.S02E02.mkv")).findFirst().get();
        assertThat(episode1.getSubtitles().stream().map(Subtitle::getLanguage).collect(toList()), allOfOrNotPresent(new String[0])); //empty value array, only empty Optional allowed
        assertThat(episode2.getSubtitles().stream().map(Subtitle::getLanguage).collect(toList()), allOfOrNotPresent(new String[0]));

    }
}