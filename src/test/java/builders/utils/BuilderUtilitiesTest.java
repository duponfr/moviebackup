package builders.utils;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class BuilderUtilitiesTest {

    @Test
    public void stripExtension() {
        assertThat(BuilderUtilities.stripExtension("Title (1991).mkv"), is("Title (1991)"));
    }
}