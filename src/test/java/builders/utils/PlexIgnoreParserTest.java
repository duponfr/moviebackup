package builders.utils;

import org.junit.Before;
import org.junit.Test;
import scanning.AbstractScanningTest;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

public class PlexIgnoreParserTest extends AbstractScanningTest {

    private PlexIgnoreParser parser;

    @Before
    public void setUp(){
        this.parser = new PlexIgnoreParser();
    }

    @Test
    public void testCorrectParsing() throws URISyntaxException {
        Path path = getPathFromTestResourceFolder("Root/Library");
        Set<String> ignoredDirectories = parser.parsePlexIgnoreIn(path);
        assertThat(ignoredDirectories, containsInAnyOrder("extras", "docs"));
    }
}