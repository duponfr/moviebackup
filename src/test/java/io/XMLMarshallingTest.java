package io;

import domain.Library;
import domain.Movie;
import domain.Root;
import org.junit.Test;
import scanning.AbstractScanningTest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class XMLMarshallingTest extends AbstractScanningTest {

    private XMLMarshalling xmlMarshalling = new XMLMarshalling();

    @Test
    public void testUnmarshalling() throws URISyntaxException, IOException {
        File xmlFile = getFileFromTestResourceFolder("xml", "Root.xml");
        Root root = xmlMarshalling.unMarshall(xmlFile.toPath());
        assertThat(root.getLabel(), is("BIFFF"));
        assertThat(root.getLibraries().size(), is(1));
        Library library = root.getLibraries().iterator().next();
        Movie movie = library.getMovies().iterator().next();
        assertThat(movie.getFileName(), is("The Living and The Dead (2006).720p.AC3-5.1.mkv"));
        assertThat(movie.getFileSize(), is(1460465778L));
        assertThat(movie.getTitle(), is("The Living and The Dead"));
        assertThat(movie.getYear(), is(2006));
    }
}