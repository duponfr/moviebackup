package reporting;

import domain.Root;
import org.junit.BeforeClass;
import org.junit.Test;
import scanning.AbstractScanningTest;
import scanning.MediaDirectoryFileVisitor;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.SortedSet;

import static java.util.stream.Collectors.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static reporting.LibraryReport.Side.*;

public class LibraryReporterTest extends AbstractScanningTest {

    private static LibraryReport report;

    @BeforeClass
    public static void setUp() throws URISyntaxException, IOException {
        Path originalPath = getPathFromTestResourceFolder("ORIGINAL");
        Path backup1Path = getPathFromTestResourceFolder("BACKUP1");
        Path backup2Path = getPathFromTestResourceFolder("BACKUP2");
        LibraryReporter reporter = new LibraryReporter();

        MediaDirectoryFileVisitor scanner_o = new MediaDirectoryFileVisitor("ORIGINAL");
        MediaDirectoryFileVisitor scanner_b1 = new MediaDirectoryFileVisitor("BACKUP1");
        MediaDirectoryFileVisitor scanner_b2 = new MediaDirectoryFileVisitor("BACKUP2");

        Files.walkFileTree(originalPath, scanner_o);
        Files.walkFileTree(backup1Path, scanner_b1);
        Files.walkFileTree(backup2Path, scanner_b2);

        Root root_o = scanner_o.getRoot();
        Root root_b1 = scanner_b1.getRoot();
        Root root_b2 = scanner_b2.getRoot();

        reporter.addRoot(root_o);
        reporter.addBackupRoot(root_b1);
        reporter.addBackupRoot(root_b2);

        report = reporter.buildReport();
    }

    @Test
    public void testLibrariesDiscovered(){
        SortedSet<String> originalLibraries = report.getExistingLibrariesForSide(ORIGINAL_NOT_IN_BACKUP);
        SortedSet<String> backupLibraries = report.getExistingLibrariesForSide(BACKUP_NOT_IN_ORIGINAL);
        SortedSet<String> commonLibraries = report.getExistingLibrariesForSide(PRESENT_IN_BOTH);

        assertThat(originalLibraries, contains("COMEDY", "GIALLO"));
        assertThat(backupLibraries, contains("OLD_STUFF"));
        assertThat(commonLibraries, contains("DRAMA", "SF", "THRILLER", "TV"));
    }

    @Test
    public void testMultipleMoviesPresent(){
        List<ReportLine> originalMultipleMovieLines =  report.getMultipleMoviesPresent(ORIGINAL_NOT_IN_BACKUP);
        List<ReportLine> backupMultipleMovieLines =  report.getMultipleMoviesPresent(BACKUP_NOT_IN_ORIGINAL);

        assertThat(report.getMultipleMoviesPresent(PRESENT_IN_BOTH), is(empty())); //we never add for PRESENT_IN_BOTH - just checking
        assertThat(originalMultipleMovieLines.size(), is(1));
        assertThat(backupMultipleMovieLines.size(), is(1));

        ReportLine originalMultipleMovieLine = originalMultipleMovieLines.get(0);
        ReportLine backupMultipleMovieLine = backupMultipleMovieLines.get(0);

        assertThat(originalMultipleMovieLine.getMessage(), is("Movie Eternal Sunshine of the Spotless Mind (2004) appears 2 times in Libraries: COMEDY, SF"));
        assertThat(backupMultipleMovieLine.getMessage(), is("Movie Christine (2016) appears 2 times in Libraries: DRAMA, THRILLER"));
    }

    @Test
    public void testMultipleTVSeriesPresent(){
        List<ReportLine> originalMultipleTVSeriesLines =  report.getMultipleTVSeriesPresent(ORIGINAL_NOT_IN_BACKUP);
        List<ReportLine> backupMultipleTVSeriesLines =  report.getMultipleTVSeriesPresent(BACKUP_NOT_IN_ORIGINAL);

        assertThat(report.getMultipleTVSeriesPresent(PRESENT_IN_BOTH), is(empty())); //we never add for PRESENT_IN_BOTH - just checking
        assertThat(originalMultipleTVSeriesLines, is(empty()));
        assertThat(backupMultipleTVSeriesLines.size(), is(1));

        ReportLine backupMultipleTVSeriesLine = backupMultipleTVSeriesLines.get(0);

        assertThat(backupMultipleTVSeriesLine.getMessage(), is("TV Series Dexter appears 2 times in Libraries: TV, TV"));
    }

    @Test
    public void testMissingMovies(){
        List<ReportLine> originalMissingInLibraryLines = report.getMissingInLibraryDifferences(ORIGINAL_NOT_IN_BACKUP, "SF");
        List<ReportLine> backupMissingInLibraryLines = report.getMissingInLibraryDifferences(BACKUP_NOT_IN_ORIGINAL, "SF");

        assertThat(originalMissingInLibraryLines.size(), is(1));
        assertThat(backupMissingInLibraryLines.size(), is(2));

        ReportLine originalMissingInLibraryLine = originalMissingInLibraryLines.get(0);
        assertThat(originalMissingInLibraryLine.getMessage(), is("In Library SF, Movie Sucker Punch (2004) is missing in ORIGINAL"));

        assertThat(backupMissingInLibraryLines.stream()
                .map(ReportLine::getMessage)
                .collect(toList()), containsInAnyOrder(
                "In Library SF, Movie 2010 (1984) is missing in BACKUP",
                "In Library SF, Movie Eternal Sunshine of the Spotless Mind (2004) is missing in BACKUP") );

        originalMissingInLibraryLines = report.getMissingInLibraryDifferences(ORIGINAL_NOT_IN_BACKUP, "THRILLER");
        assertThat(originalMissingInLibraryLines.size(), is(1));
        originalMissingInLibraryLine = originalMissingInLibraryLines.get(0);
        assertThat(originalMissingInLibraryLine.getMessage(), is("In Library THRILLER, Movie Christine (2016) is missing in ORIGINAL"));
        assertThat(report.getMissingInLibraryDifferences(BACKUP_NOT_IN_ORIGINAL, "THRILLER"), is(empty()));
        assertThat(report.getMissingInLibraryDifferences(ORIGINAL_NOT_IN_BACKUP, "DRAMA"), is(empty()));
        assertThat(report.getMissingInLibraryDifferences(BACKUP_NOT_IN_ORIGINAL, "DRAMA"), is(empty()));

        originalMissingInLibraryLines = report.getMissingInLibraryDifferences(ORIGINAL_NOT_IN_BACKUP, "DRAMA");
        backupMissingInLibraryLines = report.getMissingInLibraryDifferences(BACKUP_NOT_IN_ORIGINAL, "DRAMA");

        assertThat(originalMissingInLibraryLines, is(empty()));
        assertThat(backupMissingInLibraryLines, is(empty()));
    }

    @Test
    public void testMissingTVSeries(){
        List<ReportLine> originalMissingInLibraryLines = report.getMissingInLibraryDifferences(ORIGINAL_NOT_IN_BACKUP, "TV");
        List<ReportLine> backupMissingInLibraryLines = report.getMissingInLibraryDifferences(BACKUP_NOT_IN_ORIGINAL, "TV");

        assertThat(originalMissingInLibraryLines.size(), is(2));
        assertThat(backupMissingInLibraryLines.size(), is(1));

        ReportLine backupMissingInLibraryLine = backupMissingInLibraryLines.get(0);

        assertThat(originalMissingInLibraryLines.stream()
                .map(ReportLine::getMessage)
                .collect(toList()), containsInAnyOrder(
                "In Library TV, TV Series The Changes is missing in ORIGINAL",
                        "In Library TV, TV Series Dexter is missing in ORIGINAL") );
        assertThat(backupMissingInLibraryLine.getMessage(), is("In Library TV, TV Series The Queen's Gambit is missing in BACKUP"));
    }

    @Test
    public void testMovieDifference(){
        //SF
        List<ReportLine> movieDifferenceInLibraryLines = report.getDifferenceForMoviesInLibrary("SF");
        assertThat(movieDifferenceInLibraryLines.size(), is(1));
        ReportLine movieDifferenceInLibraryLine = movieDifferenceInLibraryLines.get(0);
        assertThat(movieDifferenceInLibraryLine.getMessage(), is("Primer (2001) : original is [Primer (2001).mkv, 17] but backup is [Primer.2001.mkv, 17]"));

        //DRAMA
        movieDifferenceInLibraryLines = report.getDifferenceForMoviesInLibrary("DRAMA");

        assertThat(movieDifferenceInLibraryLines.size(), is(3));

        assertThat(movieDifferenceInLibraryLines.stream()
                .map(ReportLine::getMessage)
                .collect(toList()), containsInAnyOrder(
                "Lost In Translation (2003) has original subtitles: [{fileName='Lost In Translation.2003.720p.DTS-5.1.dutch.srt', fileSize=47} - {fileName='Lost In Translation.2003.720p.DTS-5.1.english.srt', fileSize=49} - {fileName='Lost In Translation.2003.720p.DTS-5.1.spanish.srt', fileSize=49}] BUT has backup subtitles: [{fileName='Lost In Translation.2003.720p.DTS-5.1.dutch.srt', fileSize=54} - {fileName='Lost In Translation.2003.720p.DTS-5.1.spanish.srt', fileSize=49} - {fileName='Lost In Translation.2003.720p.DTS-5.1.srt', fileSize=49}]",
                "Contagion (2010) : original is [Contagion (2010).1080p.DTS-5.1.mkv, 34] but backup is [Contagion (2010).1080p.DTS-5.1.mkv, 20]",
                "Nine And A Half Weeks (1986) has original posters: [{fileName='Nine And A Half Weeks.jpg', fileSize=25}] BUT has backup posters: [{fileName='Nine And A Half Weeks.jpg', fileSize=25} - {fileName='Nine And A Half Weeks_v2.jpg', fileSize=28}]")
        );

        List.of("TV", "THRILLER").forEach(libraryName -> assertThat(report.getDifferenceForMoviesInLibrary(libraryName), is(empty())));
    }

    @Test
    public void testTVSeriesDifference(){
        //TV
        List<ReportLine> tvSeriesDifferenceInLibraryLines = report.getDifferenceForTVSeriesInLibrary("TV");
        assertThat(tvSeriesDifferenceInLibraryLines.size(), is(6));

        assertThat(tvSeriesDifferenceInLibraryLines.stream()
                .map(ReportLine::getMessage)
                .collect(toList()), containsInAnyOrder(
                "Blake's 7 has original posters: [{fileName='Blake's 7.jpg', fileSize=13}] BUT has no backup posters",
                "The Little Drummer Girl has original posters: [{fileName='The Little Drummer Girl.jpg', fileSize=27}] BUT has no backup posters",
                "The Little Drummer Girl.S01E01.720p.AC3-5.1.mkv has original subtitles: [{fileName='The Little Drummer Girl.S01E01.720p.AC3-5.1.srt', fileSize=47}] BUT has backup subtitles: [{fileName='The Little Drummer Girl.S01E01.720p.AC3-5.1.english.srt', fileSize=47}]",
                "The Little Drummer Girl.S01E02.720p.AC3-5.1.mkv has original subtitles: [{fileName='The Little Drummer Girl.S01E02.720p.AC3-5.1.srt', fileSize=47}] BUT has backup subtitles: [{fileName='The Little Drummer Girl.S01E02.720p.AC3-5.1.srt', fileSize=31}]",
                "TV Series The Little Drummer Girl has original episode called {fileName='The Little Drummer Girl.S01E03.720p.AC3-5.1.mkv', fileSize=47} but backup called {fileName='The Little Drummer Girl.S01E03.720p.AC3-5.1.mkv', fileSize=34}",
                "The Expanse has original posters: [{fileName='The Expanse season 1.jpg', fileSize=24} - {fileName='The Expanse season 2.jpg', fileSize=24}] BUT has backup posters: [{fileName='The Expanse season 1.jpg', fileSize=24}]"
        ));

        List.of("DRAMA", "SF", "THRILLER").forEach(libraryName -> assertThat(report.getDifferenceForTVSeriesInLibrary(libraryName), is(empty())));
    }
}