package domain;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MovieTest {

    private final String movie1 = "Arise - Subgenius Recruitment Video.1992.mpg";
    private final String movie2 = "Ms .45 (1981).720p.DTS-2.0.mkv";
    private final String movie3 = "2010 (1984).720p.AC3-5.1.mkv";
    private final String movie4 = "Ms .45.1981.720p.DTS-2.0.mkv";

    @Test
    public void testGetTitle(){
        Movie movie1 = new Movie(this.movie1, 1L);
        Movie movie2 = new Movie(this.movie2, 1L);
        Movie movie3 = new Movie(this.movie3, 1L);
        Movie movie4 = new Movie(this.movie4, 1L);
        assertThat(movie1.getTitle(), is("Arise - Subgenius Recruitment Video"));
        assertThat(movie2.getTitle(), is("Ms .45"));
        assertThat(movie3.getTitle(), is("2010"));
        assertThat(movie4.getTitle(), is("Ms .45"));
    }

    @Test
    public void testGetYear(){
        Movie movie1 = new Movie(this.movie1, 1L);
        Movie movie2 = new Movie(this.movie2, 1L);
        Movie movie3 = new Movie(this.movie3, 1L);
        Movie movie4 = new Movie(this.movie4, 1L);
        assertThat(movie1.getYear(), is(1992));
        assertThat(movie2.getYear(), is(1981));
        assertThat(movie3.getYear(), is(1984));
        assertThat(movie4.getYear(), is(1981));
    }

    @Test(expected = RuntimeException.class)
    public void testWrongFormatting(){
        new Movie("Ms .45.720p.DTS-2.0.mkv", 1L);
    }
}