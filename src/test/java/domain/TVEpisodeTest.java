package domain;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class TVEpisodeTest {

    @Test
    public void testGetSeason(){
        TVEpisode episode = new TVEpisode("episode.S01E04.mkv", 1L);
        assertThat(episode.getSeason().isPresent(), is(true));
        assertThat(episode.getSeason().get(), is(1));

        episode = new TVEpisode("episode.S01E01-E02-E03.mkv", 1L);
        assertThat(episode.getSeason().isPresent(), is(true));
        assertThat(episode.getSeason().get(), is(1));

        episode = new TVEpisode("episode.E01.mkv", 1L);
        assertThat(episode.getSeason().isPresent(), is(false));
    }

    @Test
    public void testGetEpisodeNumbers(){
        TVEpisode episode = new TVEpisode("episode.S01E04.mkv", 1L);
        assertThat(episode.getEpisodeNumbers(), is(List.of(4)));

        episode = new TVEpisode("episode.S01E01-E02.mkv", 1L);
        assertThat(episode.getEpisodeNumbers(), is(List.of(1, 2)));

        episode = new TVEpisode("episode.S01E01-E02-E03.mkv", 1L);
        assertThat(episode.getEpisodeNumbers(), is(List.of(1, 2, 3)));
    }

    @Test(expected = RuntimeException.class)
    public void testWrongFormatting(){
        new TVEpisode("episode.Se01Ep04.mkv", 1L);
    }
}