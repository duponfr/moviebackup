package scanning;

import domain.*;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class MediaDirectoryFileVisitorTest extends AbstractScanningTest{

    private final String[] expectedMovieFileNames = new String[]{
            "Film (2019).AC3-5.1.mkv",
            "Film (2018).AC3-5.1.mkv",
            "film2 (2019).m4v",
            "film3.2019.avi",
            "film1 (2019).1080p.DTS-5.1.mkv"
    };

    private final String[] expectedMoviePosters = new String[]{
            "poster_film2.png",
            "poster3.jpeg",
            "Film 1 poster.jpg"
    };

    private final String[] expectedMovieSubtitles = new String[]{
            "film2 (2019).dutch.srt",
            "film2 (2019).english.srt",
            "film2 (2019).srt",
            "film3.2019.english.srt",
            "film1 (2019).1080p.DTS-5.1.english.srt"
    };

    private final String[] expectedTVEpisodeFileNames = new String[]{
            "episode.E01.mkv",
            "episode.E02.avi",
            "episode.E03.mpg",
            "episode.E04.mpeg",
            "episode.E05.mp4",
            "episode.E06.m4v",
            "episode.S01E01-E02-E03.mkv",
            "episode.S01E04.mkv",
            "episode.S02E01.mkv",
            "episode.S02E02.mkv"
    };

    private final String[] expectedTVSeriesPosters = new String[]{
            "tv_poster.jpg",
            "season 1.jpeg",
            "season 2.png"
    };

    private final String[] expectedTVSeriesSubtitles = new String[]{
            "episode.S02E01.dutch.srt",
            "episode.S02E01.srt",
            "episode.S02E02.dutch.srt",
            "episode.S02E02.srt"
    };


    @Test
    public void testMediaDirectoryScanner() throws URISyntaxException, IOException {
        Path startDir = getPathFromTestResourceFolder("Root/");
        MediaDirectoryFileVisitor scanner = new MediaDirectoryFileVisitor("NAS");
        Files.walkFileTree(startDir, scanner);
        Root root = scanner.getRoot();
        assertThat(root.getLibraries().size(), is(1));

        Library library = root.getLibraries().iterator().next();

        //test that .plexignore file was parsed and applied
        assertThat(library.getMovies().stream()
                .map(Movie::getFileName)
                .collect(toList()), not(contains("docu (2018).mkv")));

        assertThat(library.getMovies().size(), is(5));
        assertThat(library.getTVSeries().size(), is(2));


        assertThat(library.getMovies().stream()
                .map(Movie::getFileName)
                .collect(toList()), containsInAnyOrder(expectedMovieFileNames));

        assertThat(library.getMovies().stream()
                .map(Movie::getPosters)
                .flatMap(Collection::stream)
                .map(Poster::getFileName)
                .collect(toList()), containsInAnyOrder(expectedMoviePosters));

        assertThat(library.getMovies().stream()
                .map(Movie::getSubtitles)
                .flatMap(Collection::stream)
                .map(Subtitle::getFileName)
                .collect(toList()), containsInAnyOrder(expectedMovieSubtitles));

        assertThat(library.getTVSeries().stream()
                .map(TVSeries::getName)
                .collect(toList()), containsInAnyOrder("tvseries", "tvseries2"));

        assertThat(library.getTVSeries().stream()
                .map(TVSeries::getEpisodes)
                .flatMap(Collection::stream)
                .map(TVEpisode::getFileName)
                .collect(toList()), containsInAnyOrder(expectedTVEpisodeFileNames));

        assertThat(library.getTVSeries().stream()
                .map(TVSeries::getPosters)
                .flatMap(Collection::stream)
                .map(Poster::getFileName)
                .collect(toList()), containsInAnyOrder(expectedTVSeriesPosters));

        assertThat(library.getTVSeries().stream()
                .map(TVSeries::getEpisodes)
                .flatMap(Collection::stream)
                .map(TVEpisode::getSubtitles)
                .flatMap(Collection::stream)
                .map(Subtitle::getFileName)
                .collect(toList()), containsInAnyOrder(expectedTVSeriesSubtitles));
    }
}