package scanning;

import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static scanning.MediaFileType.*;

public class MediaFileTypeRecognizerTest extends AbstractScanningTest{

    @Test
    public void testMediaFIleTypeRecognizer() throws URISyntaxException {
        File film1 = getFileFromTestResourceFolder("Root/Library/Movie1/", "film1 (2019).1080p.DTS-5.1.mkv");
        assertThat(MediaFileTypeRecognizer.recognizeMediaFileType(film1), is(FILM));
        File episode1 = getFileFromTestResourceFolder("Root/Library/tvseries/", "episode.E01.mkv");
        assertThat(MediaFileTypeRecognizer.recognizeMediaFileType(episode1), is(TV_EPISODE));
        File poster1 = getFileFromTestResourceFolder("Root/Library/tvseries/", "tv_poster.jpg");
        assertThat(MediaFileTypeRecognizer.recognizeMediaFileType(poster1), is(POSTER));
        File sub = getFileFromTestResourceFolder("Root/Library/Movie1/", "film1 (2019).1080p.DTS-5.1.english.srt");
        assertThat(MediaFileTypeRecognizer.recognizeMediaFileType(sub), is(SUBTITLE));
        File no_film = getFileFromTestResourceFolder("Root/not_Library", "dummy_file");
        assertThat(MediaFileTypeRecognizer.recognizeMediaFileType(no_film), is(UNKNOWN));
    }
}