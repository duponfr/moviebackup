package scanning;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class AbstractScanningTest {
    public static File getFileFromTestResourceFolder(String folder, String fileName) throws URISyntaxException {
        String path = folder.endsWith("/") ? folder + fileName : folder + "/" + fileName;
        return new File(Objects.requireNonNull(AbstractScanningTest.class.getClassLoader().getResource(path)).toURI());
    }

    public static Path getPathFromTestResourceFolder(String folder) throws URISyntaxException {
        return Paths.get(Objects.requireNonNull(AbstractScanningTest.class.getClassLoader().getResource(folder)).toURI());
    }
}
