package scanning.predicates;

import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PredicatesTest extends scanning.AbstractScanningTest {

    @Test
    public void testFILMPredicate() throws URISyntaxException {
        File film1 = getFileFromTestResourceFolder("Root/Library/Movie1/", "film1 (2019).1080p.DTS-5.1.mkv");
        File film2 = getFileFromTestResourceFolder("Root/Library/grouping/Movie2/", "film2 (2019).m4v");
        File film3 = getFileFromTestResourceFolder("Root/Library/grouping/Movie3/", "film3.2019.avi");
        File no_film = getFileFromTestResourceFolder("Root/not_Library", "dummy_file");

        assertThat(Predicates.FILM.test(film1), is(true));
        assertThat(Predicates.FILM.test(film2), is(true));
        assertThat(Predicates.FILM.test(film3), is(true));
        assertThat(Predicates.FILM.test(no_film), is(false));
    }

    @Test
    public void testTV_EPISODEPredicate() throws URISyntaxException {
        File episode1 = getFileFromTestResourceFolder("Root/Library/tvseries/", "episode.E01.mkv");
        File episode2 = getFileFromTestResourceFolder("Root/Library/tvseries/", "episode.E02.avi");
        File episode3 = getFileFromTestResourceFolder("Root/Library/tvseries/", "episode.E03.mpg");
        File episode4 = getFileFromTestResourceFolder("Root/Library/tvseries/", "episode.E04.mpeg");
        File episode5 = getFileFromTestResourceFolder("Root/Library/tvseries/", "episode.E05.mp4");
        File episode6 = getFileFromTestResourceFolder("Root/Library/tvseries/", "episode.E06.m4v");
        File many_episode = getFileFromTestResourceFolder("Root/Library/tvseries2/season 1/", "episode.S01E01-E02-E03.mkv");
        File no_episode = getFileFromTestResourceFolder("Root/not_Library", "dummy_file");

        assertThat(Predicates.TV_EPISODE.test(episode1), is(true));
        assertThat(Predicates.TV_EPISODE.test(episode2), is(true));
        assertThat(Predicates.TV_EPISODE.test(episode3), is(true));
        assertThat(Predicates.TV_EPISODE.test(episode4), is(true));
        assertThat(Predicates.TV_EPISODE.test(episode5), is(true));
        assertThat(Predicates.TV_EPISODE.test(episode6), is(true));
        assertThat(Predicates.TV_EPISODE.test(many_episode), is(true));
        assertThat(Predicates.TV_EPISODE.test(no_episode), is(false));
    }

    @Test
    public void testPOSTERPredicate() throws URISyntaxException {
        File poster1 = getFileFromTestResourceFolder("Root/Library/tvseries/", "tv_poster.jpg");
        File poster2 = getFileFromTestResourceFolder("Root/Library/tvseries2/", "season 1.jpeg");
        File poster3 = getFileFromTestResourceFolder("Root/Library/tvseries2/", "season 2.png");
        File no_poster = getFileFromTestResourceFolder("Root/not_Library", "dummy_file");
        assertThat(Predicates.POSTER.test(poster1), is(true));
        assertThat(Predicates.POSTER.test(poster2), is(true));
        assertThat(Predicates.POSTER.test(poster3), is(true));
        assertThat(Predicates.POSTER.test(no_poster), is(false));
    }

    @Test
    public void testSUBTITLEPredicate() throws URISyntaxException {
        File sub = getFileFromTestResourceFolder("Root/Library/Movie1/", "film1 (2019).1080p.DTS-5.1.english.srt");
        File no_sub = getFileFromTestResourceFolder("Root/not_Library", "dummy_file");
        assertThat(Predicates.SUBTITLE.test(sub), is(true));
        assertThat(Predicates.SUBTITLE.test(no_sub), is(false));
    }

    @Test
    public void testMOVIE_FOLDERPredicate() throws URISyntaxException {
        Path movie_folder = getPathFromTestResourceFolder("Root/Library/Movie1/");
        Path grouping = getPathFromTestResourceFolder("Root/Library/grouping/");
        Path no_movie_folder = getPathFromTestResourceFolder("Root/not_Library");
        assertThat(Predicates.MOVIE_FOLDER.test(movie_folder), is(true));
        assertThat(Predicates.MOVIE_FOLDER.test(grouping), is(false));
        assertThat(Predicates.MOVIE_FOLDER.test(no_movie_folder), is(false));
    }

    @Test
    public void testEPISODE_FOLDERPredicate() throws URISyntaxException {
        Path episode_folder = getPathFromTestResourceFolder("Root/Library/tvseries/");
        Path dummy_folder = getPathFromTestResourceFolder("Root/not_Library/dummy_dir");
        assertThat(Predicates.EPISODE_FOLDER.test(episode_folder), is(true));
        assertThat(Predicates.EPISODE_FOLDER.test(dummy_folder), is(false));
    }

    @Test
    public void testSEASONS_FOLDERPredicate() throws URISyntaxException {
        Path seasons_folder = getPathFromTestResourceFolder("Root/Library/tvseries2/");
        Path dummy_folder = getPathFromTestResourceFolder("Root/not_Library/dummy_dir");
        assertThat(Predicates.SEASONS_FOLDER.test(seasons_folder), is(true));
        assertThat(Predicates.SEASONS_FOLDER.test(dummy_folder), is(false));
    }

    @Test
    public void testLIBRARY_FOLDERPredicate() throws URISyntaxException {
        Path library = getPathFromTestResourceFolder("Root/Library/");
        Path no_library = getPathFromTestResourceFolder("Root/not_Library");
        Path dummy_folder = getPathFromTestResourceFolder("Root/not_Library/dummy_dir");
        assertThat(Predicates.LIBRARY_FOLDER.test(library), is(true));
        assertThat(Predicates.LIBRARY_FOLDER.test(no_library), is(false));
        assertThat(Predicates.LIBRARY_FOLDER.test(dummy_folder), is(false));
    }
}