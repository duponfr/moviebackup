package scanning.predicates;

import org.junit.Test;

import java.util.regex.Matcher;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static scanning.predicates.Predicates.*;

public class PatternsTest {

    @Test
    public void test_FILM_PATTERN(){
        String film = "Film (2009).1080p.mkv";
        assertTrue(FILM_PATTERN.asMatchPredicate().test(film));
        Matcher matcher = FILM_PATTERN.matcher(film);
        assertTrue(matcher.find());
        assertThat(matcher.group(1), is("Film"));
        assertThat(matcher.group(4), is("2009"));

    }
    @Test
    public void test_FILM_PATTERN_old_year(){
        String film = "Film.2009.1080p.mkv";
        assertTrue(FILM_PATTERN.asMatchPredicate().test(film));
        Matcher matcher = FILM_PATTERN.matcher(film);
        assertTrue(matcher.find());
        assertThat(matcher.group(1), is("Film"));
        assertThat(matcher.group(3), is("2009"));
    }

    @Test
    public void test_FILM_PATTERN_no_match(){
        String film = "Film.1080p.mkv";
        assertFalse(FILM_PATTERN.asMatchPredicate().test(film));
        Matcher matcher = FILM_PATTERN.matcher(film);
        assertFalse(matcher.find());
    }

    @Test
    public void test_TV_EPISODE_PATTERN(){
        String episode = "Dexter.S01E01.Pilot.1080p.mkv";
        assertTrue(TV_EPISODE_PATTERN.asMatchPredicate().test(episode));
        Matcher matcher = TV_EPISODE_PATTERN.matcher(episode);
        assertTrue(matcher.find());
        assertThat(matcher.group(1), is("S01"));
        assertThat(matcher.group(2), is("E01"));
    }
    @Test
    public void test_TV_EPISODE_PATTERN_2(){
        String episode = "Dexter.S01E01-E02.Pilot.1080p.mkv";
        assertTrue(TV_EPISODE_PATTERN.asMatchPredicate().test(episode));
        Matcher matcher = TV_EPISODE_PATTERN.matcher(episode);
        assertTrue(matcher.find());
        assertThat(matcher.group(1), is("S01"));
        assertThat(matcher.group(2), is("E01-E02"));
        assertThat(matcher.group(3), is("-E02"));
    }

    @Test
    public void test_TV_EPISODE_PATTERN_3(){
        String episode = "Dexter.S01E01-E02-E03.Pilot.1080p.mkv";
        assertTrue(TV_EPISODE_PATTERN.asMatchPredicate().test(episode));
        Matcher matcher = TV_EPISODE_PATTERN.matcher(episode);
        assertTrue(matcher.find());
        assertThat(matcher.group(1), is("S01"));
        assertThat(matcher.group(2), is("E01-E02-E03"));
        assertThat(matcher.group(3), is("-E02"));
        assertThat(matcher.group(4), is("-E03"));
    }

    @Test
    public void test_TV_EPISODE_PATTERN_no_season(){
        String episode = "Dexter.E01.Pilot.1080p.mkv";
        assertTrue(TV_EPISODE_PATTERN.asMatchPredicate().test(episode));
        Matcher matcher = TV_EPISODE_PATTERN.matcher(episode);
        assertTrue(matcher.find());
        assertThat(matcher.group(1), is(nullValue()));
        assertThat(matcher.group(2), is("E01"));
    }
    @Test
    public void test_TV_EPISODE_PATTERN_no_season_2(){
        String episode = "Dexter.E01-E02.Pilot.1080p.mkv";
        assertTrue(TV_EPISODE_PATTERN.asMatchPredicate().test(episode));
        Matcher matcher = TV_EPISODE_PATTERN.matcher(episode);
        assertTrue(matcher.find());
        assertThat(matcher.group(1), is(nullValue()));
        assertThat(matcher.group(2), is("E01-E02"));
        assertThat(matcher.group(3), is("-E02"));
    }
    @Test
    public void test_TV_EPISODE_PATTERN_no_season_3(){
        String episode = "Dexter.E01-E02-E03.Pilot.1080p.mkv";
        assertTrue(TV_EPISODE_PATTERN.asMatchPredicate().test(episode));
        Matcher matcher = TV_EPISODE_PATTERN.matcher(episode);
        assertTrue(matcher.find());
        assertThat(matcher.group(1), is(nullValue()));
        assertThat(matcher.group(2), is("E01-E02-E03"));
        assertThat(matcher.group(3), is("-E02"));
        assertThat(matcher.group(4), is("-E03"));
    }

    @Test
    public void test_TV_EPISODE_PATTERN_no_match(){
        String episode = "Dexter.Ep01.Pilot.1080p.mkv";
        assertFalse(TV_EPISODE_PATTERN.asMatchPredicate().test(episode));
        Matcher matcher = TV_EPISODE_PATTERN.matcher(episode);
        assertFalse(matcher.find());
    }

    @Test
    public void test_POSTER_PATTERN(){
        assertTrue(POSTER_PATTERN.asMatchPredicate().test("poster.jpg"));
        assertTrue(POSTER_PATTERN.asMatchPredicate().test("poster.jpeg"));
        assertTrue(POSTER_PATTERN.asMatchPredicate().test("poster.png"));
        assertFalse(POSTER_PATTERN.asMatchPredicate().test("poster.pico"));
    }

    @Test
    public void test_SUBTITLE_PATTERN(){
        assertTrue(SUBTITLE_PATTERN.asMatchPredicate().test("Film (2009).dutch.srt"));
        assertTrue(SUBTITLE_PATTERN.asMatchPredicate().test("Film (2009).dutch.ssa"));
        assertFalse(SUBTITLE_PATTERN.asMatchPredicate().test("Film (2009).dutch.sub"));
    }

}
