package scanning;

import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Path;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static scanning.FolderType.*;

public class FolderTypeRecognizerTest extends AbstractScanningTest{

    @Test
    public void testFolderTypeRecognizer() throws URISyntaxException {
        Path library = getPathFromTestResourceFolder("Root/Library/");
        assertThat(FolderTypeRecognizer.recognizeFolderType(library), is(LIBRARY_FOLDER));

        Path seasons_folder = getPathFromTestResourceFolder("Root/Library/tvseries2/");
        assertThat(FolderTypeRecognizer.recognizeFolderType(seasons_folder), is(SEASONS_FOLDER));

        Path episode_folder = getPathFromTestResourceFolder("Root/Library/tvseries/");
        assertThat(FolderTypeRecognizer.recognizeFolderType(episode_folder), is(EPISODE_FOLDER));

        Path movie_folder = getPathFromTestResourceFolder("Root/Library/Movie1/");
        assertThat(FolderTypeRecognizer.recognizeFolderType(movie_folder), is(MOVIE_FOLDER));

        Path movie_2part_folder = getPathFromTestResourceFolder("Root/Library/2PartMovie/");
        assertThat(FolderTypeRecognizer.recognizeFolderType(movie_2part_folder), is(MOVIE_FOLDER));

        Path movie_folder_with_extras = getPathFromTestResourceFolder("Root/Library/2PartMovie2/");
        assertThat(FolderTypeRecognizer.recognizeFolderType(movie_folder_with_extras), is(MOVIE_FOLDER));

        Path root = getPathFromTestResourceFolder("Root/");
        assertThat(FolderTypeRecognizer.recognizeFolderType(root), is(UNKNOWN));

        Path grouping = getPathFromTestResourceFolder("Root/Library/grouping/");
        assertThat(FolderTypeRecognizer.recognizeFolderType(grouping), is(UNKNOWN));

        Path unknown_folder = getPathFromTestResourceFolder("Root/not_Library");
        assertThat(FolderTypeRecognizer.recognizeFolderType(unknown_folder), is(UNKNOWN));
    }
}