package reporting;

import domain.TVEpisode;

import java.util.Collection;

public class UnequalAmountOfTVEpisodesReportLine extends ReportLine {
    private final Collection<TVEpisode> originalTVEpisodes;
    private final Collection<TVEpisode> backupTVEpisodes;

    public UnequalAmountOfTVEpisodesReportLine(String tvSeriesName, Collection<TVEpisode> originalTVEpisodes, Collection<TVEpisode> backupTVEpisodes) {
        super(LibraryReport.Side.PRESENT_IN_BOTH, tvSeriesName);
        this.originalTVEpisodes = originalTVEpisodes;
        this.backupTVEpisodes = backupTVEpisodes;
    }

    @Override
    public String getMessage() {
        return String.format("Original has %1$d episodes in tvseries %2$s but backups has %3$d", originalTVEpisodes.size(), getMediaName(), backupTVEpisodes.size());
    }
}
