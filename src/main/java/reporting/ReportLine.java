package reporting;

import domain.*;

import java.util.Collection;
import java.util.Set;

import static reporting.LibraryReport.Side;
import static reporting.LibraryReport.Side.PRESENT_IN_BOTH;

public abstract class ReportLine {

    private final Side side;
    private final String mediaName;

    public ReportLine(Side side, String mediaName) {
        this.side = side;
        this.mediaName = mediaName;
    }

    public String getMediaName() {
        return mediaName;
    }

    public Side getSide() {
        return side;
    }

    public static ReportLine ofMissingMovieInLibrary(Side side, String libraryName, String movieName){
        return new MissingMovieReportLine(side, libraryName, movieName);
    }

    public static ReportLine ofMissingTVSeriesInLibrary(Side side, String libraryName, String tvSeriesName){
        return new MissingTVSeriesReportLine(side, libraryName, tvSeriesName);
    }

    public static ReportLine ofMultipleMoviesPresent(Side side, String movieName, Collection<Library> libraries){
        return new MultipleMoviesPresentReportLine(side, movieName, libraries);
    }

    public static ReportLine ofMultipleTVSeriesPresent(Side side, String tvSeriesName, Collection<Library> libraries) {
        return new MultipleTVSeriesPresentReportLine(side, tvSeriesName, libraries);
    }

    public static ReportLine ofUnequalAmountOfTVEpisodes(String tvSeriesName, Collection<TVEpisode> originalTVEpisodes, Collection<TVEpisode> backupTVEpisodes) {
        return new UnequalAmountOfTVEpisodesReportLine(tvSeriesName, originalTVEpisodes, backupTVEpisodes);
    }

    public static ReportLine ofDifferenceForMovie(String movieName, Movie original, Movie backup){
        return new MovieDifferenceReportLine(PRESENT_IN_BOTH, movieName, original, backup);
    }

    public static ReportLine ofDifferenceForTVEpisodes(String tvSeries, TVEpisode original, TVEpisode backup){
        return new TVEpisodeDifferenceReportLine(PRESENT_IN_BOTH, tvSeries, original, backup);
    }

    public static ReportLine ofDifferenceForSubtitles(String mediaFileName, Set<Subtitle> originals, Set<Subtitle> backups) {
        return new SubtitleDifferenceReportLine(PRESENT_IN_BOTH, mediaFileName, originals, backups);
    }

    public static ReportLine ofDifferenceForPosters(String mediaFileName, Set<Poster> original, Set<Poster> backups){
        return new PosterDifferenceReportLine(PRESENT_IN_BOTH, mediaFileName, original, backups);
    }

    public abstract String getMessage();
}

