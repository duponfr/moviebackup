package reporting;

import domain.Library;

import java.util.Collection;

import static java.util.stream.Collectors.joining;
import static reporting.LibraryReport.Side;

public class MultipleMoviesPresentReportLine extends ReportLine{

    private final Collection<Library> libraries;

    public MultipleMoviesPresentReportLine(Side side, String mediaName, Collection<Library> libraries) {
        super(side, mediaName);
        this.libraries = libraries;
    }

    @Override
    public String getMessage() {
        return String.format("Movie %1$s appears %2$d times in Libraries: %3$s",
                getMediaName(), libraries.size(), libraries.stream().map(Library::getName).sorted().collect(joining(", ")));
    }
}
