package reporting;

import domain.Poster;

import java.util.Set;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.joining;
import static reporting.LibraryReport.Side;

public class PosterDifferenceReportLine extends ReportLine {

    private final Set<Poster> originals;
    private final Set<Poster> backups;

    public PosterDifferenceReportLine(Side side, String mediaName, Set<Poster> originals, Set<Poster> backups) {
        super(side, mediaName);
        this.originals = originals;
        this.backups = backups;
    }

    @Override
    public String getMessage() {
        StringBuilder bldr = new StringBuilder();
        bldr.append(getMediaName());
        if (originals.size() > 0) {
            bldr.append(" has original posters: [");
            bldr.append(this.originals.stream()
                    .sorted(comparing(Poster::getFileName))
                    .map(Poster::toString)
                    .collect(joining(" - ")));
            bldr.append("]");
        } else
            bldr.append(" has no original posters");

        bldr.append(" BUT ");
        if (backups.size() > 0) {
            bldr.append("has backup posters: [");
            bldr.append(this.backups.stream()
                    .sorted(comparing(Poster::getFileName))
                    .map(Poster::toString)
                    .collect(joining(" - ")));
            bldr.append("]");
        } else
            bldr.append("has no backup posters");

        return bldr.toString();
    }
}
