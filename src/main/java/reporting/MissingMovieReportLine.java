package reporting;

import static reporting.LibraryReport.*;

public class MissingMovieReportLine extends ReportLine{

    private final String libraryName;

    public MissingMovieReportLine(Side side, String libraryName, String mediaName) {
        super(side, mediaName);
        this.libraryName = libraryName;
    }

    @Override
    public String getMessage() {
        return String.format("In Library %1$s, Movie %2$s is missing in %3$s", this.libraryName, getMediaName(), getSide());
    }
}
