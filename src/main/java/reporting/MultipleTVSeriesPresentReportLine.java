package reporting;

import domain.Library;

import java.util.Collection;

import static java.util.stream.Collectors.joining;

public class MultipleTVSeriesPresentReportLine extends ReportLine {
    private final Collection<Library> libraries;

    public MultipleTVSeriesPresentReportLine(LibraryReport.Side side, String mediaName, Collection<Library> libraries) {
        super(side, mediaName);
        this.libraries = libraries;
    }

    @Override
    public String getMessage() {
        return String.format("TV Series %1$s appears %2$d times in Libraries: %3$s",
                getMediaName(), libraries.size(), libraries.stream().map(Library::getName).sorted().collect(joining(", ")));
    }
}