package reporting;

import builders.utils.Pair;
import com.google.common.collect.*;
import domain.*;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.*;
import static reporting.LibraryReport.Side;
import static reporting.LibraryReport.Side.*;

public class LibraryReporter {

    private final ListMultimap<String, Library> originals = ArrayListMultimap.create();
    private final ListMultimap<String, Library> backups = ArrayListMultimap.create();

    /**
     * add an original Root object te be reported on
     * call multiple times to add as many as you need
     * @param root the Root object
     * */
    public void addRoot(Root root){
        root.getLibraries().forEach(lib -> originals.put(lib.getName(), lib));
    }

    /**
     * add a backup Root object te be reported on
     * call multiple times to add as many as you need
     * @param root the Root object
     * */
    public void addBackupRoot(Root root){
        root.getLibraries().forEach(lib -> backups.put(lib.getName(), lib));
    }

    /**
     * builds and returns a LibraryReport
     * @return the LibraryReport
     * */
    public LibraryReport buildReport(){
        LibraryReport report = new LibraryReport();
        //report the names of the libraries that only exist in the originals, in the backups and the ones that are in common
        compareRoots(report);

        //check for the possibility that a movie or tv series exists in more than one location in a single root
        //after this we can safely limit ourselves to movies or tv series that exist at most once in original root or backup
        checkForMoviesAppearingMultipleTimes(report);
        checkForTVSeriesAppearingMultipleTimes(report);

        //only for the libraries in common, compare their contents
        report.getExistingLibrariesForSide(PRESENT_IN_BOTH).forEach(
                libraryName -> {
                    compareMoviesForSharedLibrary(report, libraryName);
                    compareTVSeriesForSharedLibrary(report, libraryName);
                }
        );
        return report;
    }

    /*
    identifies which libraries, based on name, exist exclusively on each side, or both
    * */
    private void compareRoots(LibraryReport report){
        EnumSet.allOf(Side.class).forEach(
                side -> {
                    switch(side){
                        case ORIGINAL_NOT_IN_BACKUP -> report.addExistingLibraryNamesForSide(side, Sets.difference(this.originals.keySet(), this.backups.keySet()));
                        case BACKUP_NOT_IN_ORIGINAL -> report.addExistingLibraryNamesForSide(side, Sets.difference(this.backups.keySet(), this.originals.keySet()));
                        case PRESENT_IN_BOTH -> report.addExistingLibraryNamesForSide(side, Sets.intersection(this.originals.keySet(), this.backups.keySet()));
                    }
                }
        );
    }

    /*
    Across all libraries in a root, look for movies with same title and year, presumably a duplicate being stored twice for no reason
    * */
    private void checkForMoviesAppearingMultipleTimes(LibraryReport report) {
        EnumSet.of(ORIGINAL_NOT_IN_BACKUP, BACKUP_NOT_IN_ORIGINAL).forEach(side -> {

            ListMultimap<String, Library> libraryMap = ORIGINAL_NOT_IN_BACKUP.equals(side) ? this.originals : this.backups;

            //mapping Libraries to Pair key representations of every movie in them, with a Pair appearing multiple times if the movie appears more than once
            ImmutableListMultimap<Library, Pair<String, Integer>> libraryPairMap = libraryMap.values().stream()
                    .collect(Collector.of(
                            ImmutableListMultimap.Builder::new,
                            (ImmutableListMultimap.Builder<Library, Pair<String, Integer>> bldr, Library library) ->
                                    bldr.putAll(library, library.getMovies().stream()
                                            .map(movie -> Pair.of(movie.getTitle(), movie.getYear())).collect(Collectors.toList())
                                    ),
                            (ImmutableListMultimap.Builder<Library, Pair<String, Integer>> bldr1, ImmutableListMultimap.Builder<Library, Pair<String, Integer>> bldr2)
                                    -> bldr1.putAll(bldr2.build()),
                            ImmutableListMultimap.Builder::build));

            //a Multiset maintains a count of how many times the same element was added; this will allow us to identify Movies that appear more than once
            Multiset<Pair<String, Integer>> multisetOriginals = HashMultiset.create();
            multisetOriginals.addAll(libraryPairMap.values());

            //reverse map that will allow us toe easily look up all libraries that a 'multiple' movie sits in
            ImmutableListMultimap<Pair<String, Integer>, Library> inverseOriginalsMap = libraryPairMap.inverse();

            multisetOriginals.entrySet().stream()
                    //filter out Pairs that appear more than once
                    .filter(entry -> entry.getCount()>1)
                    .map(Multiset.Entry::getElement)
                    //now that we know the Pairs we want, we only need them once
                    .collect(toSet())
                    .forEach(pair -> report.addMultipleMoviePresent(
                            ReportLine.ofMultipleMoviesPresent(side, String.format("%1$s (%2$d)", pair.getFirst(), pair.getSecond()), inverseOriginalsMap.get(pair) )));
        });
    }

    /*
    Across all libraries in a root, look for tv series with same name, presumably a duplicate being stored twice for no reason
    * */
    private void checkForTVSeriesAppearingMultipleTimes(LibraryReport report) {
        EnumSet.of(ORIGINAL_NOT_IN_BACKUP, BACKUP_NOT_IN_ORIGINAL).forEach(side -> {

            ListMultimap<String, Library> libraryMap = ORIGINAL_NOT_IN_BACKUP.equals(side) ? this.originals : this.backups;

            //mapping Libraries to tv series in them, with a tv series appearing multiple times if the movie appears more than once
            ImmutableListMultimap<Library, String> libraryPairMap = libraryMap.values().stream()
                    .collect(Collector.of(
                            ImmutableListMultimap.Builder::new,
                            (ImmutableListMultimap.Builder<Library, String> bldr, Library library) ->
                                    bldr.putAll(library, library.getTVSeries().stream()
                                            .map(TVSeries::getName).collect(Collectors.toList())
                                    ),
                            (ImmutableListMultimap.Builder<Library, String> bldr1, ImmutableListMultimap.Builder<Library, String> bldr2)
                                    -> bldr1.putAll(bldr2.build()),
                            ImmutableListMultimap.Builder::build));

            //a Multiset maintains a count of how many times the same element was added; this will allow us to identify tv series that appear more than once
            Multiset<String> multisetOriginals = HashMultiset.create();
            multisetOriginals.addAll(libraryPairMap.values());

            //reverse map that will allow us toe easily look up all libraries that a 'multiple' movie sits in
            ImmutableListMultimap<String, Library> inverseOriginalsMap = libraryPairMap.inverse();

            multisetOriginals.entrySet().stream()
                    //filter out tv series that appear more than once
                    .filter(entry -> entry.getCount()>1)
                    .map(Multiset.Entry::getElement)
                    //now that we know the Pairs we want, we only need them once
                    .collect(toSet())
                    .forEach(name -> report.addMultipleTVSeriesPresent(
                            ReportLine.ofMultipleTVSeriesPresent(side, name, inverseOriginalsMap.get(name) )));
        });
    }

    /*
    For a given Library (logically it should be one shared between original and backup root) report the following:
        Movies existing in original but not backup
        Movies existing in backup but not original
        compare Movies that appear only once, both in original and backup
    * */
    private void compareMoviesForSharedLibrary(LibraryReport report, String libraryName){

        ListMultimap<Pair<String, Integer>, Movie> originalPairMoviesMap = getPairMovieMultimapForLibrary(libraryName, this.originals);
        ListMultimap<Pair<String, Integer>, Movie> backupPairMoviesMap = getPairMovieMultimapForLibrary(libraryName, this.backups);

        Sets.SetView<Pair<String, Integer>> newMoviePairs = Sets.difference(originalPairMoviesMap.keySet(), backupPairMoviesMap.keySet());
        Sets.SetView<Pair<String, Integer>> deletedMoviePairs = Sets.difference(backupPairMoviesMap.keySet(), originalPairMoviesMap.keySet());
        Sets.SetView<Pair<String, Integer>> sharedMoviePairs = Sets.intersection(originalPairMoviesMap.keySet(), backupPairMoviesMap.keySet());

        //covers the case of a movie not existing in the backup
        report.addMissingInLibraryDifference(libraryName,
                newMoviePairs.stream()
                        .map(originalPairMoviesMap::get)
                        .flatMap(Collection::stream)
                        .map(movie -> ReportLine.ofMissingMovieInLibrary(BACKUP_NOT_IN_ORIGINAL, libraryName, movie.toString()))
                        .collect(toList())
        );

        //covers the case of a deleted movie still being backed up
        report.addMissingInLibraryDifference(libraryName,
                deletedMoviePairs.stream()
                        .map(backupPairMoviesMap::get)
                        .flatMap(Collection::stream)
                        .map(movie -> ReportLine.ofMissingMovieInLibrary(ORIGINAL_NOT_IN_BACKUP, libraryName, movie.toString()))
                        .collect(toList())
        );

        sharedMoviePairs.forEach(
                pair -> {
                    List<Movie> originalMovies = originalPairMoviesMap.get(pair);
                    List<Movie> backupMovies = backupPairMoviesMap.get(pair);
                    //the cases where the same movie appears more than once (or not at all) in the original or backup library has already been reported
                    //this covers the more usual case that the original and backup both exist but don't match somehow
                    if(originalMovies.size() == 1 && backupMovies.size() == 1) {
                        Movie originalMovie = originalMovies.get(0);
                        Movie backupMovie = backupMovies.get(0);
                        reportDifferenceBetween2Movies(report, libraryName, originalMovie, backupMovie);
                    }
                }
        );
    }

    /*
    For a given Library (logically it should be one shared between original and backup root) report the following:
        TV Series existing in original but not backup
        TV Series existing in backup but not original
        compare TV Series that appear only once, both in original and backup
    * */
    private void compareTVSeriesForSharedLibrary(LibraryReport report, String libraryName){

        ListMultimap<String, TVSeries> originalNameTVSeriesMap = getNameTVSeriesMultimapForLibrary(libraryName, this.originals);
        ListMultimap<String, TVSeries> backupNameTVSeriesMap = getNameTVSeriesMultimapForLibrary(libraryName, this.backups);

        Sets.SetView<String> newTVSeriesNames = Sets.difference(originalNameTVSeriesMap.keySet(), backupNameTVSeriesMap.keySet());
        Sets.SetView<String> deletedTVSeriesNames = Sets.difference(backupNameTVSeriesMap.keySet(), originalNameTVSeriesMap.keySet());
        Sets.SetView<String> sharedTVSeriesNames = Sets.intersection(originalNameTVSeriesMap.keySet(), backupNameTVSeriesMap.keySet());

        //covers the case of a tv series not existing in the backup
        report.addMissingInLibraryDifference(libraryName,
                newTVSeriesNames.stream()
                        .map(tvSeriesName -> ReportLine.ofMissingTVSeriesInLibrary(BACKUP_NOT_IN_ORIGINAL, libraryName, tvSeriesName))
                        .collect(toList())
        );

        //covers the case of a deleted tvseries still being backed up
        report.addMissingInLibraryDifference(libraryName,
                deletedTVSeriesNames.stream()
                        .map(tvSeriesName -> ReportLine.ofMissingTVSeriesInLibrary(ORIGINAL_NOT_IN_BACKUP, libraryName, tvSeriesName))
                        .collect(toList())
        );

        sharedTVSeriesNames.forEach(tvSeriesName -> {
            List<TVSeries> originalTVSeries = originalNameTVSeriesMap.get(tvSeriesName);
            List<TVSeries> backupTVSeries = backupNameTVSeriesMap.get(tvSeriesName);
            //the cases where the same tv series appears more than once (or not at all) in the original or backup library has already been reported
            //this covers the more usual case that the original and backup both exist but don't match somehow
            if(originalTVSeries.size() == 1 && backupTVSeries.size() == 1) {
                TVSeries originalTV = originalTVSeries.get(0);
                TVSeries backupTV = backupTVSeries.get(0);
                reportDifferenceBetween2TVSeries(report, libraryName, originalTV, backupTV);
            }
        });
    }

    /*
    Comparing two movies, this method will report:
        if both movies do not have identical filename and size
        if all subtitles of these 2 movies do not have identical filename and size
        if all posters of these 2 movies do not have identical filename and size
    * */
    private void reportDifferenceBetween2Movies(LibraryReport report, String libraryName, Movie originalMovie, Movie backupMovie) {
        if(!originalMovie.equals(backupMovie)){
            ReportLine line = ReportLine.ofDifferenceForMovie(originalMovie.toString(), originalMovie, backupMovie);
            report.addDifferenceForMovieInLibrary(libraryName, line);
        }
        Set<Subtitle> originalSubtitles = originalMovie.getSubtitles();
        Set<Subtitle> backupSubtitles = backupMovie.getSubtitles();
        if(!originalSubtitles.equals(backupSubtitles)){
            ReportLine line = ReportLine.ofDifferenceForSubtitles(originalMovie.toString(), originalSubtitles, backupSubtitles);
            report.addDifferenceForMovieInLibrary(libraryName, line);
        }

        Set<Poster> originalPosters = originalMovie.getPosters();
        Set<Poster> backupPosters = backupMovie.getPosters();
        if(!originalPosters.equals(backupPosters)){
            ReportLine line = ReportLine.ofDifferenceForPosters(originalMovie.toString(), originalPosters, backupPosters);
            report.addDifferenceForMovieInLibrary(libraryName, line);
        }
    }

    /*
    Comparing two tv series, this method will report:
        if both tv series do not have the same amount of episodes in them - in which case further reporting stops
        if they do have a the same amount of episodes, it will compare these episodes one-on-one (making a reasonable guess as to which should be compared to which),
        and report for each two matching episodes:
            if both episodes do not have identical filename and size
            if all subtitles of these 2 movies do not have identical filename and size
            if all posters of these 2 movies do not have identical filename and size
    * */
    private void reportDifferenceBetween2TVSeries(LibraryReport report, String libraryName, TVSeries originalTVSeries, TVSeries backupTVSeries) {
        SortedSet<Poster> originalPosters = new TreeSet<>(originalTVSeries.getPosters());
        SortedSet<Poster> backupPosters = new TreeSet<>(backupTVSeries.getPosters());
        if(!originalPosters.equals(backupPosters)){
            ReportLine line = ReportLine.ofDifferenceForPosters(originalTVSeries.getName(), originalPosters, backupPosters);
            report.addDifferenceForTVSeriesInLibrary(libraryName, line);
        }

        //by sorting the episodes by episode numbers we are virtually certain to line up the originals and backup ones correctly
        SortedSet<TVEpisode> originalTVEpisodes = originalTVSeries.getEpisodes().stream()
                .sorted(comparing( (TVEpisode ep1) -> ep1.getEpisodeNumbers().stream().map(Object::toString).collect(joining("-")) ))
                .collect(toCollection(TreeSet::new));
        SortedSet<TVEpisode> backupTVEpisodes = backupTVSeries.getEpisodes().stream()
                .sorted(comparing( (TVEpisode ep1) -> ep1.getEpisodeNumbers().stream().map(Object::toString).collect(joining("-")) ))
                .collect(toCollection(TreeSet::new));

        //if we couldn't line up one-on-one, report and give up on further reporting
        if(originalTVEpisodes.size() != backupTVEpisodes.size()){
            ReportLine line = ReportLine.ofUnequalAmountOfTVEpisodes(originalTVSeries.getName(), originalTVEpisodes, backupTVEpisodes);
            report.addUnequalAmountOfTVEpisodes(libraryName, line);
        }
        //from here on we are guaranteed that both iterators have the same amount of elements
        else{
            Iterator<TVEpisode> it1 = originalTVEpisodes.iterator();
            Iterator<TVEpisode> it2 = backupTVEpisodes.iterator();

            while(it1.hasNext()){
                TVEpisode original = it1.next();
                TVEpisode backup = it2.next();
                if(!original.equals(backup)){
                    ReportLine line = ReportLine.ofDifferenceForTVEpisodes(originalTVSeries.getName(), original, backup);
                    report.addDifferenceForTVSeriesInLibrary(libraryName, line);
                }

                SortedSet<Subtitle> originalSubtitles = new TreeSet<>(original.getSubtitles());
                SortedSet<Subtitle> backupSubtitles = new TreeSet<>(backup.getSubtitles());
                if(!originalSubtitles.equals(backupSubtitles)){
                    ReportLine line = ReportLine.ofDifferenceForSubtitles(original.getFileName(), originalSubtitles, backupSubtitles);
                    report.addDifferenceForTVSeriesInLibrary(libraryName, line);
                }
            }
        }
    }

    private ListMultimap<Pair<String, Integer>, Movie> getPairMovieMultimapForLibrary(String libraryName, ListMultimap<String, Library> libraryMap) {
        return libraryMap.get(libraryName).stream()
                    .map(Library::getMovies)
                    .flatMap(Collection::stream)
                    .collect(
                            ArrayListMultimap::create,
                            (map, movie) -> map.put(Pair.of(movie.getTitle(), movie.getYear()), movie),
                            ArrayListMultimap::putAll);
    }

    private ListMultimap<String, TVSeries> getNameTVSeriesMultimapForLibrary(String libraryName, ListMultimap<String, Library> libraryMap) {
        return libraryMap.get(libraryName).stream()
                .map(Library::getTVSeries)
                .flatMap(Collection::stream)
                .collect(
                        ArrayListMultimap::create,
                        (map, tvseries) -> map.put(tvseries.getName(), tvseries),
                        ArrayListMultimap::putAll);
    }
}
