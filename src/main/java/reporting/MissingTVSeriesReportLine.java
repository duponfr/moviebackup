package reporting;

public class MissingTVSeriesReportLine extends ReportLine {

    private final String libraryName;

    public MissingTVSeriesReportLine(LibraryReport.Side side, String libraryName, String tvSeriesName) {
        super(side, tvSeriesName);
        this.libraryName = libraryName;
    }

    @Override
    public String getMessage() {
        return String.format("In Library %1$s, TV Series %2$s is missing in %3$s", this.libraryName, getMediaName(), getSide());
    }
}
