package reporting;

import com.google.common.collect.*;

import java.util.*;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;
import static reporting.LibraryReport.Side.PRESENT_IN_BOTH;

public class LibraryReport {

    public enum Side {
        ORIGINAL_NOT_IN_BACKUP("ORIGINAL"), BACKUP_NOT_IN_ORIGINAL("BACKUP"), PRESENT_IN_BOTH("BOTH");
        private final String label;

        Side(String label) {
            this.label = label;
        }

        @Override
        public String toString(){
            return this.label;
        }
    }

    private final SortedSetMultimap<Side, String> existingLibraries = TreeMultimap.create();
    private final ListMultimap<Side, ReportLine> multipleMoviesPresent = ArrayListMultimap.create();
    private final ListMultimap<Side, ReportLine> multipleTVSeriesPresent = ArrayListMultimap.create();
    private final ListMultimap<String, ReportLine> missingInLibrary = ArrayListMultimap.create();
    private final ListMultimap<String, ReportLine> unequalAmountOfTVEpisodes = ArrayListMultimap.create();
    private final ListMultimap<String, ReportLine> differencesForMovie = ArrayListMultimap.create();
    private final ListMultimap<String, ReportLine> differencesForTVSeries = ArrayListMultimap.create();

    /**
     * Returns an alphabetically sorted List of Library names as present in one side or the other
     * @param side the Side to compare
     * @return a SortedSet of Library names depending on the side chosen. For...
     *      ORIGINAL_NOT_IN_BACKUP : return Library names that exist in original roots but not in the backups
     *      BACKUP_NOT_IN_ORIGINAL : return library names that exist in backup roots but not in originals
     *      PRESENT_IN_BOTH : return library names present in both originals and backups
     * */
    public SortedSet<String> getExistingLibrariesForSide(Side side){
        return this.existingLibraries.get(side);
    }

    /**
     * Returns an alphabetically sorted List of ReportLines representing a missing Movie or TVSeries in one side or the other of a common library
     *
     * @param side the Side to compare
     * @param libraryName the name of the common library
     * @return a List of ReportLines for the given library and side
     *      ORIGINAL_NOT_IN_BACKUP : return missing in original roots but present in the backups
     *      BACKUP_NOT_IN_ORIGINAL : return missing in backup roots but present in originals
     *      PRESENT_IN_BOTH : return present in both originals and backups
     * */
    public List<ReportLine> getMissingInLibraryDifferences(Side side, String libraryName){
        return this.missingInLibrary.get(libraryName).stream()
                .filter(line -> side.equals(line.getSide()))
                .sorted(comparing(ReportLine::getMediaName))
                .collect(toList());
    }

    /**
     * Returns a List of ReportLines representing a Movie that is present in a common library of original and backup but differs between both in some way
     * @param libraryName the name of the common library
     * @return a List of ReportLines for the given library
     * */
    public List<ReportLine> getDifferenceForMoviesInLibrary(String libraryName){
        return this.differencesForMovie.get(libraryName).stream()
                .filter(line -> PRESENT_IN_BOTH.equals(line.getSide()))
                .collect(toList());
    }

    /**
     * Returns a List of ReportLines representing a TVSeries that is present in a common library of original and backup but differs between both in some way
     * @param libraryName the name of the common library
     * @return a List of ReportLines for the given library
     * */
    public List<ReportLine> getDifferenceForTVSeriesInLibrary(String libraryName){
        return this.differencesForTVSeries.get(libraryName).stream()
                .filter(line -> PRESENT_IN_BOTH.equals(line.getSide()))
                .collect(toList());
    }

    public List<ReportLine> getMultipleMoviesPresent(Side side){
        return this.multipleMoviesPresent.get(side);
    }

    public List<ReportLine> getMultipleTVSeriesPresent(Side side){
        return this.multipleTVSeriesPresent.get(side);
    }



    /**
     * Adds a Collection of library names that are present in the given side of the roots
     * multiple calls will not remove the previously added Libraries but adding the same name twice will have no effect
     *
     * @param side the Side on which the library names exist
     * */
    public void addExistingLibraryNamesForSide(Side side, Collection<String> libraryNames){
        this.existingLibraries.putAll(side, libraryNames);
    }

    /**
     * Adds a Collection of ReportLines representing a missing Movie or TVSeries between the original and backup of a particular Library
     * (While it is possible to also represent a shared occurrence, using Side PRESENT_IN_BOTH, there is probably no use case for that)
     *
     * @param libraryName the name of the library
     * @param lines a Collection of ReportLines
     * */
    public void addMissingInLibraryDifference(String libraryName, Collection<ReportLine> lines) {
        this.missingInLibrary.putAll(libraryName, lines);
    }

    public void addMultipleMoviePresent(ReportLine reportLine) {
        this.multipleMoviesPresent.put(reportLine.getSide(), reportLine);
    }

    public void addMultipleTVSeriesPresent(ReportLine reportLine){
        this.multipleTVSeriesPresent.put(reportLine.getSide(), reportLine);
    }

    public void addUnequalAmountOfTVEpisodes(String libraryName, ReportLine reportLine){
        this.unequalAmountOfTVEpisodes.put(libraryName, reportLine);
    }

    public void addDifferenceForMovieInLibrary(String libraryName, ReportLine line){
        this.differencesForMovie.put(libraryName, line);
    }

    public void addDifferenceForTVSeriesInLibrary(String libraryName, ReportLine line){
        this.differencesForTVSeries.put(libraryName, line);
    }
}
