package reporting;

import domain.Movie;

import static reporting.LibraryReport.*;

public class MovieDifferenceReportLine extends ReportLine {

    private final Movie original;
    private final Movie backup;

    public MovieDifferenceReportLine(Side side, String movieName, Movie original, Movie backup) {
        super(side, movieName);
        this.original = original;
        this.backup = backup;
    }

    @Override
    public String getMessage() {
        return String.format("%1$s : original is [%2$s, %3$d] but backup is [%4$s, %5$d]",
                original.toString(), original.getFileName(), original.getFileSize(), backup.getFileName(), backup.getFileSize());
    }
}
