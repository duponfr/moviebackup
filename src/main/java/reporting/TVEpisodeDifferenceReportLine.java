package reporting;

import domain.TVEpisode;

import static reporting.LibraryReport.Side;

class TVEpisodeDifferenceReportLine extends ReportLine {

    private final TVEpisode original;
    private final TVEpisode backup;

    public TVEpisodeDifferenceReportLine(Side side, String tvSeries, TVEpisode original, TVEpisode backup) {
        super(side, tvSeries);
        this.original = original;
        this.backup = backup;
    }

    @Override
    public String getMessage() {
        return String.format("TV Series %1$s has original episode called %2$s but backup called %3$s", getMediaName(), original, backup);
    }
}
