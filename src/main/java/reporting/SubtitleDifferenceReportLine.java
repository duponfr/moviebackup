package reporting;

import domain.Subtitle;

import java.util.Comparator;
import java.util.Set;

import static java.util.stream.Collectors.joining;
import static reporting.LibraryReport.*;

public class SubtitleDifferenceReportLine extends ReportLine {

    private final Set<Subtitle> originals;
    private final Set<Subtitle> backups;

    public SubtitleDifferenceReportLine(Side side, String mediaName, Set<Subtitle> originals, Set<Subtitle> backups) {
        super(side, mediaName);
        this.originals = originals;
        this.backups = backups;
    }

    @Override
    public String getMessage() {
        StringBuilder bldr = new StringBuilder();
        bldr.append(getMediaName());
        if (originals.size() > 0) {
            bldr.append(" has original subtitles: [");
            bldr.append(this.originals.stream()
                    .sorted(Comparator.comparing(Subtitle::getFileName))
                    .map(Subtitle::toString)
                    .collect(joining(" - ")));
            bldr.append("]");
        } else
            bldr.append(" has no original subtitles");

        bldr.append(" BUT ");
        if (backups.size() > 0) {
            bldr.append("has backup subtitles: [");
            bldr.append(this.backups.stream()
                    .sorted(Comparator.comparing(Subtitle::getFileName))
                    .map(Subtitle::toString)
                    .collect(joining(" - ")));
            bldr.append("]");
        } else
            bldr.append("no backup subtitles");

        return bldr.toString();
    }
}
