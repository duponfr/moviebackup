package builders;

import domain.Movie;
import domain.Poster;
import domain.Subtitle;
import scanning.predicates.Predicates;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static builders.utils.BuilderUtilities.stripExtension;
import static java.util.Objects.isNull;

public class MovieBuilder {

    private final Pattern multiplePartMoviePattern = Pattern.compile("\\.(CD|Part\\s)\\d\\.", Pattern.CASE_INSENSITIVE);

    private String fileName = null;
    private Long size = 0L;
    private final List<Object[]> subtitleFiles = new ArrayList<>();
    private final List<Object[]> posterFiles = new ArrayList<>();

    private MovieBuilder(){}

    public static MovieBuilder getNewBuilder(){
        return new MovieBuilder();
    }

    public MovieBuilder addMovieFile(File file) throws IOException {
        if(!Predicates.FILM.test(file))
            throw new RuntimeException(String.format("File %1$s is not a movie file", file.getName()));

        if(!isNull(this.fileName)){
            checkMultiplePartMovie(file.getName());
        }
        this.fileName = stripMultiPartFromFileName(file.getName());
        this.size += Files.size(file.toPath());

        return this;
    }

    public MovieBuilder addSubtitleFile(File file) throws IOException {
        if(!Predicates.SUBTITLE.test(file))
            throw new RuntimeException(String.format("File %1$s is not a subtitle file",file.getName()));

        this.subtitleFiles.add(makePair(file));

        return this;
    }

    public MovieBuilder addPosterFile(File file) throws IOException {
        if(!Predicates.POSTER.test(file))
            throw new RuntimeException(String.format("File %1$s is not a poster file", file.getName()));

        this.posterFiles.add(makePair(file));

        return this;
    }

    private Object[] makePair(File file) throws IOException {
        Object[] pair = new Object[2];
        pair[0] = file.getName();
        pair[1] = Files.size(file.toPath());
        return pair;
    }

    /*
    This method will strip the 'CD1' part of the filename, present in movies that come in multiple parts
     * */
    private String stripMultiPartFromFileName(String fileName){
        Matcher matcher = multiplePartMoviePattern.matcher(fileName);
        if(matcher.find())
            return fileName.replace(matcher.group(), ".");
        else
            return fileName;
    }

    /*
    This method will throw an exception if a multi-part movie's filename does not match the previous part
    * */
    private void checkMultiplePartMovie(String nextFileName){
        if(!this.fileName.equals(stripMultiPartFromFileName(nextFileName)))
            throw new RuntimeException(String.format("MovieBuilder already has a fileName %1$s, cannot add %2$s", this.fileName, nextFileName));
    }

    private String extractLanguage(String fileName) {
        String[] splits = fileName.replace(stripExtension(this.fileName), "").split("\\.");
        if(splits.length > 2)
            return splits[1];
        else
            return null;
    }

    public Movie build(){
        Movie movie = new Movie(this.fileName, this.size);
        this.posterFiles.stream()
                .map(o -> new Poster((String) o[0], (Long) o[1]))
                .forEach(movie::addPoster);
        this.subtitleFiles.stream()
                .map( o -> new Subtitle( (String) o[0], (Long) o[1], extractLanguage((String) o[0])) )
                .forEach(movie::addSubtitle);
        return movie;
    }
}
