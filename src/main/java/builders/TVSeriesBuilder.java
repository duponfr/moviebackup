package builders;

import domain.Poster;
import domain.Subtitle;
import domain.TVEpisode;
import domain.TVSeries;
import scanning.predicates.Predicates;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

import static builders.utils.BuilderUtilities.stripExtension;
import static java.util.Comparator.comparing;

public class TVSeriesBuilder {

    private String name = null;

    private final List<Object[]> episodeFiles = new ArrayList<>();
    private final List<Object[]> subtitleFiles = new ArrayList<>();
    private final List<Object[]> posterFiles = new ArrayList<>();

    private TVSeriesBuilder() {}

    public static TVSeriesBuilder getNewBuilder(){
        return new TVSeriesBuilder();
    }

    public TVSeriesBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public TVSeriesBuilder addEpisodeFile(File file) throws IOException {
        if(!Predicates.TV_EPISODE.test(file))
            throw new RuntimeException(String.format("File %1$s is not a tv episode file", file.getName()));

        this.episodeFiles.add(makePair(file));
        return this;
    }

    public TVSeriesBuilder addSubtitleFile(File file) throws IOException {
        if(!Predicates.SUBTITLE.test(file))
            throw new RuntimeException(String.format("File %1$s is not a subtitle file", file.getName()));

        this.subtitleFiles.add(makePair(file));
        return this;
    }

    public TVSeriesBuilder addPosterFile(File file) throws IOException {
        if(!Predicates.POSTER.test(file))
            throw new RuntimeException(String.format("File %1$s is not a poster file", file.getName()));

        this.posterFiles.add(makePair(file));
        return this;
    }

    private Object[] makePair(File file) throws IOException {
        Object[] pair = new Object[2];
        pair[0] = file.getName();
        pair[1] = Files.size(file.toPath());
        return pair;
    }

    private String extractLanguage(String fileName, String subtitleName) {
        String[] splits = subtitleName.replace(stripExtension(fileName), "").split("\\.");
        if(splits.length > 2)
            return splits[1];
        else
            return null;
    }

    public TVSeries build(){

        TVSeries tvSeries = new TVSeries(this.name);

        this.posterFiles.stream()
                .map(o -> new Poster((String) o[0], (Long) o[1]))
                .forEach(tvSeries::addPoster);

        //this alphabetical sorting guarantees that episode and subtitle files that belong to each other, align with each other in their respective collections
        this.episodeFiles.sort(comparing(o -> (String) o[0]));
        this.subtitleFiles.sort(comparing(o -> (String) o[0]));
        Deque<Object[]> sortedSubtitles = new LinkedList<>(this.subtitleFiles);

        this.episodeFiles.stream()
                .map( (Object[] o) -> {
                    var name = (String) o[0];
                    var size = (Long) o[1];
                    TVEpisode episode = new TVEpisode(name,size);
                    while( sortedSubtitles.size()>0 && ((String) sortedSubtitles.peek()[0]).startsWith(stripExtension(name))){
                        Object[] subO = sortedSubtitles.pop();
                        var subName = (String) subO[0];
                        var subSize = (Long) subO[1];
                        var language = extractLanguage(name, subName);
                        episode.addSubtitle(new Subtitle(subName, subSize, language));
                    }
                    return episode;
                })
                .forEach(tvSeries::addTVEpisode);
        return tvSeries;
    }
}
