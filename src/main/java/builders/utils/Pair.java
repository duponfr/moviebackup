package builders.utils;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

@Immutable
public class Pair<T extends Comparable<T>, Y extends Comparable<Y>>  implements Comparable<Pair<T, Y>>{
    private final T t;
    private final Y y;

    private Pair(T t, Y y){
        requireNonNull(t);
        requireNonNull(y);
        this.t = t;
        this.y = y;
    }

    public static <T extends Comparable<T>, Y extends Comparable<Y>> Pair<T, Y> of(T t, Y y){
        return new Pair<>(t, y);
    }

    public T getFirst() {
        return this.t;
    }

    public Y getSecond() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return t.equals(pair.t) && y.equals(pair.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(t, y);
    }

    @Override
    public int compareTo(Pair<T, Y> p2) {
        return this.getFirst().compareTo(p2.getFirst()) == 0 ? this.getSecond().compareTo(p2.getSecond()) : this.getFirst().compareTo(p2.getFirst());
    }
}
