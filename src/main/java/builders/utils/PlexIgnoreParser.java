package builders.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.isNull;

public class PlexIgnoreParser {

    private static final String PLEX_IGNORE_FILENAME = ".plexignore";
    private static final Pattern ignoredPattern = Pattern.compile("\\*(\\w+)/\\*");

    private static final Logger logger = LoggerFactory.getLogger(PlexIgnoreParser.class.getName());

    /**
     * parses lines from a .plexignore File looking for the pattern '*name/*' and returns a List of these names
     * @param path the Path under which to look for the .plexignore file
     * @return a Set of String with matching names
     * */
    public Set<String> parsePlexIgnoreIn(Path path){
        Set<String> result = new HashSet<>();

        File[] files = path.toFile().listFiles();
        if(!isNull(files)){
            Optional<File> plexignoreOptional = Arrays.stream(files).filter(file -> PLEX_IGNORE_FILENAME.equals(file.getName())).findFirst();
            if(plexignoreOptional.isPresent()) {
                logger.trace(String.format("parsing %1$s in %2$s", PLEX_IGNORE_FILENAME, path));
                try (BufferedReader reader = new BufferedReader (new FileReader(plexignoreOptional.get()))) {
                    String line;
                    while((line=reader.readLine())!=null){
                        Matcher matcher = ignoredPattern.matcher(line);
                        if(matcher.find()) {
                            result.add(matcher.group(1));
                        }
                    }
                } catch (FileNotFoundException e) {
                    logger.warn("Did not find .plexignore file in " + path, e);
                } catch (IOException e) {
                    logger.warn("Could not read .plexignore file in " + path, e);
                }
            }
        }
        return result;
    }
}
