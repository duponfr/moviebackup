package builders.utils;

public class BuilderUtilities {

    public static String stripExtension(String fileName){
        return fileName.substring(0, fileName.length()-4);
    }
}
