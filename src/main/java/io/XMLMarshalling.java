package io;

import com.thoughtworks.xstream.XStream;
import domain.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;

public class XMLMarshalling {

    private final XStream xStream = new XStream();

    public XMLMarshalling() {
        setupXStreamSecurity();
        setupXStreamAliases();
    }

    public void marshall(Root root, Path outputFile) throws IOException {
        if(Files.exists(outputFile))
            Files.delete(outputFile);
        try(BufferedWriter writer = Files.newBufferedWriter(outputFile, StandardCharsets.UTF_8, WRITE, CREATE);){
            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
            writer.newLine();
            writer.write(this.xStream.toXML(root));
        }
    }

    public Root unMarshall(Path inputFile) throws IOException {
        try(BufferedReader reader = Files.newBufferedReader(inputFile, StandardCharsets.UTF_8)){
            return (Root) this.xStream.fromXML(reader);
        }
    }

    private void setupXStreamSecurity() {
        Class<?>[] classes = new Class[] { Root.class, Library.class, Movie.class, TVSeries.class, TVEpisode.class, Poster.class, Subtitle.class };
        XStream.setupDefaultSecurity(xStream);
        xStream.allowTypes(classes);
    }

    private void setupXStreamAliases() {
        this.xStream.alias("Root", Root.class);
        this.xStream.useAttributeFor(Root.class, "label");
        this.xStream.aliasField("label", Root.class, "label");

        this.xStream.alias("Library", Library.class);
        this.xStream.useAttributeFor(Library.class, "name");
        this.xStream.aliasField("name", Library.class, "name");

        this.xStream.alias("Movie", Movie.class);
        this.xStream.aliasField("title", Library.class, "title");
        this.xStream.aliasField("year", Library.class, "year");
        this.xStream.omitField(Movie.class, "logger");

        this.xStream.alias("TVSeries", TVSeries.class);
        this.xStream.useAttributeFor(TVSeries.class, "name");
        this.xStream.aliasField("name", TVSeries.class, "name");

        this.xStream.alias("TVEpisode", TVEpisode.class);
        this.xStream.aliasField("season", Library.class, "season");
        this.xStream.aliasField("episodeNumbers", Library.class, "episodeNumbers");
        this.xStream.omitField(TVEpisode.class, "logger");

        this.xStream.alias("Poster", Poster.class);

        this.xStream.alias("Subtitle", Subtitle.class);
        this.xStream.useAttributeFor(Subtitle.class, "language");
        this.xStream.aliasField("language", Subtitle.class, "language");
    }
}
