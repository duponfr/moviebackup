package scanning;

import scanning.predicates.Predicates;

import java.io.File;

import static scanning.MediaFileType.*;

public class MediaFileTypeRecognizer {
    public static MediaFileType recognizeMediaFileType(File file){
        if(Predicates.TV_EPISODE.test(file))
            return TV_EPISODE;
        //the order is important here as a TV EPISODE is easily confused with a FILM (it is a more specific version of the FILM pattern)
        if (Predicates.FILM.test(file))
            return FILM;
        if(Predicates.POSTER.test(file))
            return POSTER;
        if(Predicates.SUBTITLE.test(file))
            return SUBTITLE;
        return UNKNOWN;
    }
}
