package scanning;

import builders.MovieBuilder;
import builders.TVSeriesBuilder;
import builders.utils.PlexIgnoreParser;
import domain.Library;
import domain.Root;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Set;

import static java.nio.file.FileVisitResult.*;
import static java.util.Objects.isNull;
import static scanning.FolderType.*;

public class MediaDirectoryFileVisitor implements FileVisitor<Path> {

    private final String label;
    private final ArrayDeque<FolderType> deque = new ArrayDeque<>();

    private final Logger logger = LoggerFactory.getLogger(MediaDirectoryFileVisitor.class);
    private static final PlexIgnoreParser plexIgnoreParser = new PlexIgnoreParser();

    private final Root root;
    private Library library;
    private MovieBuilder movieBuilder;
    private TVSeriesBuilder tvSeriesBuilder;

    private Set<String> plexIgnoreFolders = new HashSet<>();

    public MediaDirectoryFileVisitor(String label){
        this.label = label;
        root = new Root(getLabel());
    }

    public String getLabel() {
        return label;
    }

    public Root getRoot() {
        return root;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs){
        FolderType parentType = deque.peek();
        FolderType folderType = FolderTypeRecognizer.recognizeFolderType(dir);

        FileVisitResult result = CONTINUE;

        switch (folderType){
            case LIBRARY_FOLDER -> {
                if(isNull(parentType)){
                    //started inside library folder
                    logger.info("Starting scan from inside LIBRARY FOLDER {}", dir.toFile().getAbsolutePath());
                    logger.trace("push {} on deque", LIBRARY_FOLDER);
                    deque.push(LIBRARY_FOLDER);
                    this.library = new Library(dir.toFile().getName());
                    parsePlexIgnoreFile(dir);
                }
                else {
                    switch (parentType) {
                        case ROOT, UNKNOWN -> {
                            logger.info("Scanning LIBRARY FOLDER {} inside {}", dir.toFile().getAbsolutePath(), parentType);
                            logger.trace("push {} on deque", LIBRARY_FOLDER);
                            deque.push(LIBRARY_FOLDER);
                            this.library = new Library(dir.toFile().getName());
                            parsePlexIgnoreFile(dir);
                        }
                        case LIBRARY_FOLDER, MOVIE_FOLDER, SEASONS_FOLDER, EPISODE_FOLDER ->
                            throw new RuntimeException(String.format("Found LIBRARY_FOLDER %1$s inside parent Type %2$s",
                                    dir.toFile().getAbsolutePath(), parentType));
                    }
                }
            }
            case MOVIE_FOLDER -> {
                if(isNull(parentType) || outsideLibrary() || onIgnoreList(dir)){
                    logger.info("Skipping MOVIE_FOLDER {} inside {}", dir.toFile().getAbsolutePath(), parentType);
                    result = SKIP_SUBTREE;
                }
                else {
                    switch (parentType) {
                        case LIBRARY_FOLDER, UNKNOWN -> {
                            logger.info("Scanning MOVIE_FOLDER {} inside {}", dir.toFile().getAbsolutePath(), parentType);
                            logger.trace("push {} on deque", MOVIE_FOLDER);
                            deque.push(MOVIE_FOLDER);
                            this.movieBuilder = MovieBuilder.getNewBuilder();
                        }
                        case MOVIE_FOLDER, SEASONS_FOLDER, EPISODE_FOLDER -> {
                            logger.info("Skipping probably extras folder {} inside {}", dir.toFile().getAbsolutePath(), parentType);
                            result = SKIP_SUBTREE;
                        }
                    }
                }
            }
            case SEASONS_FOLDER -> {
                if(isNull(parentType) || outsideLibrary() || onIgnoreList(dir)){
                    logger.info("Skipping SEASONS_FOLDER {} inside {}", dir.toFile().getAbsolutePath(), parentType);
                    result = SKIP_SUBTREE;
                }
                else {
                    switch (parentType) {
                        case LIBRARY_FOLDER, UNKNOWN -> {
                            logger.info("Scanning SEASONS_FOLDER {} inside {}", dir.toFile().getAbsolutePath(), parentType);
                            logger.trace("push {} on deque", SEASONS_FOLDER);
                            deque.push(SEASONS_FOLDER);
                            this.tvSeriesBuilder = TVSeriesBuilder.getNewBuilder();
                            this.tvSeriesBuilder.setName(dir.toFile().getName());
                        }
                        case MOVIE_FOLDER, SEASONS_FOLDER, EPISODE_FOLDER ->
                                throw new RuntimeException(String.format("Found SEASONS_FOLDER %1$s inside %2$s", dir.toFile().getAbsolutePath(), parentType));
                    }
                }
            }
            case EPISODE_FOLDER -> {
                if(isNull(parentType) || outsideLibrary() || onIgnoreList(dir)){
                    logger.info("Skipping EPISODE_FOLDER {} inside {}", dir.toFile().getAbsolutePath(), parentType);
                    result = SKIP_SUBTREE;
                }
                else {
                    switch (parentType) {
                        case LIBRARY_FOLDER, UNKNOWN -> {
                            logger.info("Scanning EPISODE_FOLDER {} inside {}", dir.toFile().getAbsolutePath(), parentType);
                            logger.trace("push {} on deque", EPISODE_FOLDER);
                            deque.push(EPISODE_FOLDER);
                            this.tvSeriesBuilder = TVSeriesBuilder.getNewBuilder();
                            this.tvSeriesBuilder.setName(dir.toFile().getName());
                        }
                        case SEASONS_FOLDER -> {
                            logger.info("Scanning EPISODE_FOLDER {} inside {}", dir.toFile().getAbsolutePath(), parentType);
                            logger.trace("push {} on deque", EPISODE_FOLDER);
                            logger.trace("Already created TVSeriesBuilder");
                            deque.push(EPISODE_FOLDER);
                        }
                        case MOVIE_FOLDER, EPISODE_FOLDER ->
                                throw new RuntimeException(String.format("Found EPISODE_FOLDER %1$s inside %2$s", dir.toFile().getAbsolutePath(), parentType));
                    }
                }
            }
            case UNKNOWN -> {
                if(isNull(parentType)){
                    logger.info("Starting scan from ROOT");
                    logger.trace("push {} on deque", ROOT);
                    deque.push(ROOT);
                }
                else {
                    switch (parentType) {
                        case ROOT, LIBRARY_FOLDER, UNKNOWN -> {
                            logger.info("Passing through UNKNOWN folder {} inside {}", dir.toFile().getAbsolutePath(), parentType);
                            logger.trace("push {} on deque", UNKNOWN);
                            deque.push(UNKNOWN);
                        }
                        case MOVIE_FOLDER, SEASONS_FOLDER, EPISODE_FOLDER -> {
                            logger.info("Skipping probably extras folder {} inside {}", dir.toFile().getAbsolutePath(), parentType);
                            result = SKIP_SUBTREE;
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
        FolderType parentType = deque.peek();
        if(isNull(parentType)){
            throw new RuntimeException("visiting file without parent type " + path.toFile().getAbsolutePath());
        }

        FileVisitResult result = CONTINUE;

        //as long as folders that group libraries do not contain any files, we can safely skip the part of the directory tree as soon as we do find a file outside a Library
        //we'll allow for hidden system files though, which might reside in a parent folder whose children down the line are Libraries
        if (!path.toFile().isHidden() && outsideLibrary()) {
            logger.info("Skipping siblings of {}", path.toFile().getAbsolutePath());
            result = SKIP_SIBLINGS;
        }
        else {
            MediaFileType mediaFileType = MediaFileTypeRecognizer.recognizeMediaFileType(path.toFile());
            switch(mediaFileType){
                case TV_EPISODE -> {
                    switch(parentType){
                        case EPISODE_FOLDER -> {
                            logger.info("Processing TV_EPISODE {} in {} ", path.toFile().getAbsolutePath(), parentType);
                            this.tvSeriesBuilder.addEpisodeFile(path.toFile());
                        }

                        case ROOT, LIBRARY_FOLDER, MOVIE_FOLDER, SEASONS_FOLDER, UNKNOWN ->
                                logger.warn("Ignoring TV_EPISODE {} in {}", path.toFile().getAbsolutePath(), parentType);
                    }
                }
                case FILM -> {
                    switch(parentType){
                        case MOVIE_FOLDER -> {
                            logger.info("Processing FILM {} in {} ", path.toFile().getAbsolutePath(), parentType);
                            this.movieBuilder.addMovieFile(path.toFile());
                        }
                        case ROOT, LIBRARY_FOLDER, EPISODE_FOLDER, SEASONS_FOLDER, UNKNOWN ->
                                logger.warn("Ignoring FILM {} in {}", path.toFile().getAbsolutePath(), parentType);
                    }
                }
                case POSTER -> {
                    switch(parentType){
                        case MOVIE_FOLDER -> {
                            logger.info("Processing POSTER {} in {} ", path.toFile().getAbsolutePath(), parentType);
                            this.movieBuilder.addPosterFile(path.toFile());
                        }
                        case SEASONS_FOLDER, EPISODE_FOLDER -> {
                            logger.info("Processing POSTER {} in {} ", path.toFile().getAbsolutePath(), parentType);
                            this.tvSeriesBuilder.addPosterFile(path.toFile());
                        }
                        case ROOT, LIBRARY_FOLDER, UNKNOWN ->
                                logger.warn("Ignoring POSTER {} in {}", path.toFile().getAbsolutePath(), parentType);
                    }
                }
                case SUBTITLE -> {
                    switch(parentType){
                        case MOVIE_FOLDER -> {
                            logger.info("Processing SUBTITLE {} in {} ", path.toFile().getAbsolutePath(), parentType);
                            this.movieBuilder.addSubtitleFile(path.toFile());
                        }
                        case EPISODE_FOLDER -> {
                            logger.info("Processing SUBTITLE {} in {} ", path.toFile().getAbsolutePath(), parentType);
                            this.tvSeriesBuilder.addSubtitleFile(path.toFile());
                        }
                        case ROOT, LIBRARY_FOLDER, SEASONS_FOLDER, UNKNOWN ->
                                logger.warn("Ignoring SUBTITLE {} in {}", path.toFile().getAbsolutePath(), parentType);
                    }
                }
                case UNKNOWN -> logger.info("Ignoring UNKNOWN file {} in {}", path.toFile().getAbsolutePath(), parentType);
            }
        }
        return result;
    }

    @Override
    public FileVisitResult visitFileFailed(Path path, IOException exc) {
        logger.warn("failed visiting file {}, continuing...", path.toFile().getAbsolutePath(), exc);
        return CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        FolderType folderType = deque.pop();
        logger.trace("Popping {} from deque", folderType);
        logger.info("Leaving {} {}" , folderType, dir.toFile().getAbsolutePath());

        switch (folderType){
            case MOVIE_FOLDER -> this.library.addMovie(this.movieBuilder.build());
            case SEASONS_FOLDER -> this.library.addTVSeries(this.tvSeriesBuilder.build());
            case EPISODE_FOLDER -> {
                if(!SEASONS_FOLDER.equals(deque.peek())) this.library.addTVSeries(this.tvSeriesBuilder.build());
            }
            case LIBRARY_FOLDER -> this.root.addLibrary(this.library);
        }
        return CONTINUE;
    }

    private void parsePlexIgnoreFile(Path path){
        this.plexIgnoreFolders = Set.copyOf(plexIgnoreParser.parsePlexIgnoreIn(path));
    }

    private boolean onIgnoreList(Path path){
        return this.plexIgnoreFolders.contains(path.toFile().getName());
    }

    private boolean outsideLibrary(){
        return this.deque.stream().noneMatch(LIBRARY_FOLDER::equals);
    }
}
