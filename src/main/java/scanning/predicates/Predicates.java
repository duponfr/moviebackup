package scanning.predicates;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import static java.util.Objects.nonNull;
import static java.util.function.Predicate.not;

public interface Predicates {

    Logger logger = LoggerFactory.getLogger(Predicates.class);

    File[] EMPTY_FILE_ARRAY = new File[0];

    Pattern FILM_PATTERN = Pattern.compile("^(.+)(\\.(\\d{4})|\\s\\((\\d{4})\\))\\..*(mkv|avi|mpg|mpeg|mp4|m4v)$");
    Pattern TV_EPISODE_PATTERN = Pattern.compile(".+?(S\\d{2})?(E\\d{2}(-E\\d{2})?(-E\\d{2})?).*\\.(mkv|avi|mpg|mpeg|mp4|m4v)$");
    Pattern POSTER_PATTERN = Pattern.compile(".+\\.(jpg|jpeg|png)$");
    Pattern SUBTITLE_PATTERN = Pattern.compile(".+\\.(srt|ssa)$");

    Predicate<File> FILM = file -> file.isFile() && FILM_PATTERN.asMatchPredicate().test(file.getName());
    Predicate<File> TV_EPISODE = file -> file.isFile() && TV_EPISODE_PATTERN.asMatchPredicate().test(file.getName());
    Predicate<File> POSTER = file -> file.isFile() && POSTER_PATTERN.asMatchPredicate().test(file.getName());
    Predicate<File> SUBTITLE = file -> file.isFile() && SUBTITLE_PATTERN.asMatchPredicate().test(file.getName());
    Predicate<File> EXTRAS = file -> file.isDirectory() && "extras".equals(file.getName());

    Predicate<Path> MOVIE_FOLDER = (Path path) -> {

        boolean result = false;
        try {
            result = Files.isDirectory(path)
                    && Arrays.stream(Optional.ofNullable(path.toFile().listFiles(File::isFile)).orElse(EMPTY_FILE_ARRAY))
                    .filter(not(File::isHidden))
                    .anyMatch(FILM.and(not(TV_EPISODE)));
        } catch (SecurityException e) {
            logger.info("MOVIE_FOLDER Predicate threw SecurityException for path {}", path);
            logger.debug("", e);
        }
        return result;
    };

    Predicate<Path> EPISODE_FOLDER = (Path path) -> {

        boolean result = false;
        try {
            File[] files = path.toFile().listFiles(f -> !f.isHidden());
            result = Files.isDirectory(path)
                    && nonNull(files)
                    && files.length > 0
                    && Arrays.stream(files)
                    .filter(not(POSTER).and(not(SUBTITLE)).and(not(EXTRAS)))
                    .count()>0
                 &&
                    Arrays.stream(files)
                            .filter(not(POSTER).and(not(SUBTITLE)).and(not(EXTRAS)))
                    .allMatch(TV_EPISODE);
        } catch (SecurityException e) {
            logger.info("EPISODE_FOLDER Predicate threw SecurityException for path {}", path);
            logger.debug("", e);
        }
        return result;
    };

    Predicate<Path> SEASONS_FOLDER = (Path path) -> {

        boolean result = false;
        try {
            File[] files = path.toFile().listFiles(f -> !f.isHidden() && f.isDirectory());
            result = Files.isDirectory(path)
                    && nonNull(files)
                    && files.length > 0
                    && Arrays.stream(files)
                    .map(File::toPath)
                    .allMatch(EPISODE_FOLDER);
        } catch (SecurityException e) {
            logger.info("SEASONS_FOLDER Predicate threw SecurityException for path {}", path);
            logger.debug("", e);
        }
        return result;
    };

    Predicate<Path> LIBRARY_FOLDER = (Path path) -> {

        boolean result = false;
        try {
            result = Files.isDirectory(path)
                    && Arrays.stream(Optional.ofNullable(path.toFile().listFiles(File::isFile)).orElse(EMPTY_FILE_ARRAY))
                    .filter(not(File::isHidden))
                    .anyMatch(f -> f.getName().equals(".plexignore"));
        } catch (SecurityException e) {
            logger.info("LIBRARY_FOLDER Predicate threw SecurityException for path {}", path);
            logger.debug("", e);
        }
        return result;
    };
}
