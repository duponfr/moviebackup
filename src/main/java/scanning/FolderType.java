package scanning;

public enum FolderType {
    ROOT, LIBRARY_FOLDER, MOVIE_FOLDER, EPISODE_FOLDER, SEASONS_FOLDER, UNKNOWN
}
