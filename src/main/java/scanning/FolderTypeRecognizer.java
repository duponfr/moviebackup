package scanning;

import scanning.predicates.Predicates;

import java.nio.file.Path;

import static scanning.FolderType.*;

public class FolderTypeRecognizer {

    public static FolderType recognizeFolderType(Path path){
        if (Predicates.LIBRARY_FOLDER.test(path))
            return LIBRARY_FOLDER;
        if(Predicates.SEASONS_FOLDER.test(path))
            return SEASONS_FOLDER;
        if(Predicates.EPISODE_FOLDER.test(path))
            return EPISODE_FOLDER;
        if(Predicates.MOVIE_FOLDER.test(path))
            return MOVIE_FOLDER;
        return UNKNOWN;
    }
}
