package scanning;

public enum MediaFileType {
    FILM, TV_EPISODE, POSTER , SUBTITLE, UNKNOWN
}
