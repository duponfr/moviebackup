import domain.Root;
import io.XMLMarshalling;
import reporting.LibraryReport;
import reporting.LibraryReporter;
import reporting.ReportLine;
import scanning.MediaDirectoryFileVisitor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.SortedSet;

public class Main {

    public static void main(String[] args) {
        try {
            //createMediaXMLFIle();

            compareRoots();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void compareRoots() throws IOException {
        Path originalGFile = Paths.get("D:\\MediaReports\\original\\G-Disk.xml");
        Path originalHFile = Paths.get("D:\\MediaReports\\original\\ASIA-Disk.xml");

        Path backupCieFile = Paths.get("D:\\MediaReports\\backup\\LaCie.xml");
        Path backupCieSFile = Paths.get("D:\\MediaReports\\backup\\LaCie-S.xml");
        Path backupWDBookFile = Paths.get("D:\\MediaReports\\backup\\WDBook.xml");
        Path backupANIMEFile = Paths.get("D:\\MediaReports\\backup\\ANIME.xml");

        XMLMarshalling xmlMarshalling = new XMLMarshalling();

        Root originalGFileRoot = xmlMarshalling.unMarshall(originalGFile);
        Root originalHRoot = xmlMarshalling.unMarshall(originalHFile);

        Root backupCieRoot = xmlMarshalling.unMarshall(backupCieFile);
        Root backupCieSRoot = xmlMarshalling.unMarshall(backupCieSFile);
        Root backupWDBookRoot = xmlMarshalling.unMarshall(backupWDBookFile);
        Root backupANIMERoot = xmlMarshalling.unMarshall(backupANIMEFile);

        LibraryReporter reporter = new LibraryReporter();

        reporter.addRoot(originalGFileRoot);
        reporter.addRoot(originalHRoot);

        reporter.addBackupRoot(backupCieRoot);
        reporter.addBackupRoot(backupCieSRoot);
        reporter.addBackupRoot(backupWDBookRoot);
        reporter.addBackupRoot(backupANIMERoot);

        LibraryReport report = reporter.buildReport();

        System.out.println("START");
        System.out.println("MISSING LIBRARIES IN ORIGINAL");
        System.out.println("----------------------------");
        report.getExistingLibrariesForSide(LibraryReport.Side.BACKUP_NOT_IN_ORIGINAL).stream().forEach(System.out::println);
        System.out.println("");
        System.out.println("MISSING LIBRARIES IN BACKUP");
        System.out.println("----------------------------");
        report.getExistingLibrariesForSide(LibraryReport.Side.ORIGINAL_NOT_IN_BACKUP).stream().forEach(System.out::println);
        System.out.println("");
        System.out.println("DUPLICATE MOVIES IN ORIGINAL");
        System.out.println("----------------------------");
        printMessagesOfReportLines(
                report.getMultipleMoviesPresent(LibraryReport.Side.ORIGINAL_NOT_IN_BACKUP)
        );
        System.out.println("");
        System.out.println("DUPLICATE MOVIES IN BACKUP");
        System.out.println("----------------------------");
        printMessagesOfReportLines(
                report.getMultipleMoviesPresent(LibraryReport.Side.BACKUP_NOT_IN_ORIGINAL)
        );

        SortedSet<String> commonLibraries = report.getExistingLibrariesForSide(LibraryReport.Side.PRESENT_IN_BOTH);

        for(String commonLibrary : commonLibraries){
            System.out.println("");
            System.out.println("LIBRARY: " + commonLibrary);
            System.out.println("--------------------------------------------------------");

            System.out.println("UNNECESSARY BACKED UP MOVIES OR TV SERIES");
            System.out.println("------------------------------------------");
            printMessagesOfReportLines(
                    report.getMissingInLibraryDifferences(LibraryReport.Side.ORIGINAL_NOT_IN_BACKUP, commonLibrary)
            );

            System.out.println("");
            System.out.println("NOT BACKED UP MOVIES OR TV SERIES");
            System.out.println("---------------------------------");
            printMessagesOfReportLines(
                    report.getMissingInLibraryDifferences(LibraryReport.Side.BACKUP_NOT_IN_ORIGINAL, commonLibrary)
            );

            System.out.println("");
            System.out.println("CHANGED MOVIES");
            System.out.println("----------------------------");
            printMessagesOfReportLines(
                    report.getDifferenceForMoviesInLibrary(commonLibrary)
            );

            System.out.println("");
            System.out.println("CHANGED TV SERIES");
            System.out.println("----------------------------");
            printMessagesOfReportLines(
                    report.getDifferenceForTVSeriesInLibrary(commonLibrary)
            );
        }
    }

    private static void printMessagesOfReportLines(List<ReportLine> lines){
        lines.stream()
             .map(ReportLine::getMessage)
             .forEach(System.out::println);
    }

    public static void createMediaXMLFIle() throws IOException {

//        DONE Path mediaDirectory = Paths.get("G:\\");
//        Path outputFile = Paths.get("D:\\MediaReports\\original\\G-Disk.xml");
//        String label = "G-Disk";

        Path mediaDirectory = Paths.get("F:\\");
        Path outputFile = Paths.get("D:\\MediaReports\\original\\ASIA-Disk.xml");
        String label = "ASIA-Disk";


//        DONE Path mediaDirectory = Paths.get("G:\\");
//        Path outputFile = Paths.get("D:\\MediaReports\\backup\\ANIME.xml");
//        String label = "ANIME";

//        DONE Path mediaDirectory = Paths.get("Z:\\");
//        Path outputFile = Paths.get("D:\\MediaReports\\backup\\WDBook.xml");
//        String label = "WDBook";

//        DONE Path mediaDirectory = Paths.get("H:\\");        //CAREFUL!!!
//        Path outputFile = Paths.get("D:\\MediaReports\\backup\\LaCie-S.xml");
//        String label = "LaCie-S";
//
//        Path mediaDirectory = Paths.get("H:\\");        //CAREFUL!!!
//        Path outputFile = Paths.get("D:\\MediaReports\\backup\\LaCie.xml");
//        String label = "LaCie";

        MediaDirectoryFileVisitor visitor = new MediaDirectoryFileVisitor(label);
        Files.walkFileTree(mediaDirectory, visitor);
        Root originalRoot = visitor.getRoot();

        XMLMarshalling xmlMarshalling = new XMLMarshalling();
        xmlMarshalling.marshall(originalRoot, outputFile);

    }
}
