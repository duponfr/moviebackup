package domain;

import java.util.ArrayList;
import java.util.Collection;

import static java.util.Objects.requireNonNull;

/**
 * Do not override equals() on this class using only the name as criteria. It is entirely possible for different libraries to have the same name.
 * */
public class Library {
    private final String name;
    private final Collection<Movie> movies = new ArrayList<>();
    private final Collection<TVSeries> tvseries = new ArrayList<>();

    public Library(String name) {
        requireNonNull(name);
        this.name = name;
    }

    public void addMovie(Movie movie){
        this.movies.add(movie);
    }

    public void addTVSeries(TVSeries tvseries){
        this.tvseries.add(tvseries);
    }

    public String getName() {
        return name;
    }

    public Collection<Movie> getMovies() {
        return movies;
    }

    public Collection<TVSeries> getTVSeries() {
        return tvseries;
    }
}
