package domain;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

public abstract class AbstractMediaFile {

    private final String fileName;
    private final Long fileSize;

    protected AbstractMediaFile(String fileName, Long fileSize) {
        requireNonNull(fileName);
        requireNonNull(fileSize);
        this.fileName = fileName;
        this.fileSize = fileSize;
    }

    public String getFileName() {
        return fileName;
    }

    public Long getFileSize() {
        return fileSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractMediaFile that = (AbstractMediaFile) o;
        return fileName.equals(that.fileName) && fileSize.equals(that.fileSize);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileName, fileSize);
    }

    @Override
    public String toString() {
        return "{" +
                "fileName='" + fileName + '\'' +
                ", fileSize=" + fileSize +
                '}';
    }
}
