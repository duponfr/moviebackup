package domain;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;

import static java.util.Objects.isNull;
import static scanning.predicates.Predicates.TV_EPISODE_PATTERN;

public class TVEpisode extends AbstractMediaFile implements Comparable<TVEpisode>{

    private final Integer season;
    private final List<Integer> episodeNumbers = new ArrayList<>();
    private final Collection<Subtitle> subtitles = new ArrayList<>();

    @SuppressWarnings("all")
    private final Logger logger = LoggerFactory.getLogger(TVEpisode.class);

    public TVEpisode(String fileName, Long fileSize) {
        super(fileName, fileSize);
        Matcher matcher = TV_EPISODE_PATTERN.matcher(getFileName());
        if(!matcher.find()){
            logger.warn("TVEpisode {} does not match episode pattern", getFileName());
            throw new RuntimeException(String.format("TVEpisode %1$s does not match episode pattern", getFileName()));
        }
        else {
            if (!isNull(matcher.group(1)))
                this.season = Integer.valueOf(matcher.group(1).substring(1, 3));
            else this.season = null;

            int groupIndex = 2;
            while(groupIndex <= 4 && !isNull(matcher.group(groupIndex))){
                this.episodeNumbers.add((Integer.valueOf(matcher.group(groupIndex).substring(Math.min(groupIndex-1, 2), Math.min(groupIndex+1, 4)))));
                groupIndex++;
            }
        }
    }

    public void addSubtitle(Subtitle subtitle){
        this.subtitles.add(subtitle);
    }

    public Collection<Subtitle> getSubtitles() {
        return subtitles;
    }

    public Optional<Integer> getSeason(){
        return Optional.ofNullable(this.season);
    }

    public List<Integer> getEpisodeNumbers(){
        return this.episodeNumbers;
    }

    @Override
    public int compareTo(TVEpisode ep) {
        return ComparisonChain.start()
                .compare(this.getFileName(), ep.getFileName(), Ordering.natural() )
                .compare(this.getFileSize(), ep.getFileSize())
                .result();
    }
}
