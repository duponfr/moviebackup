package domain;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;

public class Poster extends AbstractMediaFile implements Comparable<Poster>{

    public Poster(String fileName, Long fileSize) {
        super(fileName, fileSize);
    }

    @Override
    public int compareTo(Poster p) {
        return ComparisonChain.start()
                .compare(this.getFileName(), p.getFileName(), Ordering.natural() )
                .compare(this.getFileSize(), p.getFileSize())
                .result();
    }
}
