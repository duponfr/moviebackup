package domain;

import java.util.ArrayList;
import java.util.Collection;

import static java.util.Objects.requireNonNull;

public class Root {
    private final String label;
    private final Collection<Library> libraries = new ArrayList<>();

    public Root(String label) {
        requireNonNull(label);
        this.label = label;
    }

    public void addLibrary(Library library){
        this.libraries.add(library);
    }

    public String getLabel() {
        return label;
    }

    public Collection<Library> getLibraries() {
        return libraries;
    }
}
