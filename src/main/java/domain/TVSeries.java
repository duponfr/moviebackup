package domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class TVSeries {
    private final String name;
    private final Collection<TVEpisode> episodes = new ArrayList<>();
    private final Collection<Poster> posters = new ArrayList<>();

    public TVSeries(String name){
        requireNonNull(name);
        this.name = name;
    }

    public void addPoster(Poster poster){
        this.posters.add(poster);
    }

    public void addTVEpisode(TVEpisode episode){
        this.episodes.add(episode);
    }

    public String getName() {
        return name;
    }

    public Collection<TVEpisode> getEpisodes() {
        return episodes;
    }

    public Collection<Poster> getPosters() {
        return posters;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TVSeries tvSeries = (TVSeries) o;
        return name.equals(tvSeries.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
