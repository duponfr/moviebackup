package domain;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;

import java.util.Optional;

public class Subtitle extends AbstractMediaFile implements Comparable<Subtitle>{

    private final String language;

    public Subtitle(String fileName, Long fileSize, String language) {
        super(fileName, fileSize);
        this.language = language;
    }

    public Optional<String> getLanguage(){
        return Optional.ofNullable(this.language);
    }

    @Override
    public int compareTo(Subtitle s) {
        return ComparisonChain.start()
                .compare(this.getFileName(), s.getFileName(), Ordering.natural() )
                .compare(this.getFileSize(), s.getFileSize())
                .result();
    }
}
