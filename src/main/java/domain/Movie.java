package domain;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;

import static java.util.Objects.isNull;
import static scanning.predicates.Predicates.FILM_PATTERN;

public class Movie extends AbstractMediaFile implements Comparable<Movie>{

    private final String title;
    private final Integer year;

    private final Set<Subtitle> subtitles = new HashSet<>();
    private final Set<Poster> posters = new HashSet<>();

    @SuppressWarnings("all")
    private final Logger logger = LoggerFactory.getLogger(Movie.class);

    public Movie(String fileName, Long fileSize) {
        super(fileName, fileSize);
        Matcher matcher = FILM_PATTERN.matcher(getFileName());
        if(!matcher.find()){
            logger.warn("Movie {} does not match movie pattern", getFileName());
            throw new RuntimeException(String.format("Movie %1$s does not match movie pattern", getFileName()));
        }
        else {
            this.title = matcher.group(1);
            if(!isNull(matcher.group(3))){
                logger.warn("Movie {} uses OLD year pattern. Please FIX.", getFileName());
                this.year = Integer.valueOf(matcher.group(3));
            }
            else
                this.year = Integer.valueOf(matcher.group(4));
        }
    }

    public void addSubtitle(Subtitle subtitle){
        this.subtitles.add(subtitle);
    }

    public void addPoster(Poster poster){
        this.posters.add(poster);
    }

    public Set<Subtitle> getSubtitles() {
        return subtitles;
    }

    public Set<Poster> getPosters() {
        return posters;
    }

    public String getTitle(){
        return this.title;
    }

    public Integer getYear(){
        return this.year;
    }

    @Override
    public String toString() {
        return String.format("%1$s (%2$s)", getTitle(), getYear().toString());
    }

    @Override
    public int compareTo(Movie m) {
        return ComparisonChain.start()
                .compare(this.getFileName(), m.getFileName(), Ordering.natural() )
                .compare(this.getFileSize(), m.getFileSize())
                .result();
    }
}
